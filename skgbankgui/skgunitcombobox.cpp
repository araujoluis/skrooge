/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file defines classes SKGUnitComboBox.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgunitcombobox.h"

#include <klocalizedstring.h>

#include "skgdocumentbank.h"
#include "skgmainpanel.h"
#include "skgunitvalueobject.h"

SKGUnitComboBox::SKGUnitComboBox(QWidget* iParent): SKGComboBox(iParent), m_document(nullptr), m_fillWhereClause(QStringLiteral("t_type!='I'"))
{
}

SKGUnitComboBox::~SKGUnitComboBox()
{
    m_document = nullptr;
}

void SKGUnitComboBox::setDocument(SKGDocumentBank* iDocument)
{
    m_document = iDocument;
    connect(m_document, &SKGDocument::tableModified, this, &SKGUnitComboBox::dataModified);
    dataModified(QLatin1String(""), 0);
}

void SKGUnitComboBox::setWhereClauseCondition(const QString& iCondition)
{
    m_fillWhereClause = iCondition;
    dataModified(QLatin1String(""), 0);
}

SKGUnitObject SKGUnitComboBox::getUnit()
{
    SKGUnitObject unit(m_document);
    QString unitName = text();
    if ((m_document != nullptr) && !unitName.isEmpty()) {
        SKGError err;
        err = unit.setSymbol(unitName);
        if (!unit.exist()) {
            IFOKDO(err, unit.setName(unitName))
            IFOKDO(err, unit.save())

            SKGUnitValueObject unitVal;
            IFOKDO(err, unit.addUnitValue(unitVal))
            IFOKDO(err, unitVal.setDate(QDate::currentDate()))
            IFOKDO(err, unitVal.setQuantity(1))
            IFOKDO(err, unitVal.save())

            IFOK(err) m_document->sendMessage(i18nc("An information message",  "Unit '%1' has been created", text()), SKGDocument::Positive);
        } else {
            err = unit.load();
        }
    }
    return unit;
}

void SKGUnitComboBox::setUnit(const SKGUnitObject& iUnit)
{
    if (text() != iUnit.getSymbol()) {
        setText(iUnit.getSymbol());

        emit unitChanged();
    }
}

void SKGUnitComboBox::refershList()
{
    // Fill comboboxes
    if (m_document != nullptr) {
        SKGMainPanel::fillWithDistinctValue(QList<QWidget*>() << this, m_document, QStringLiteral("unit"), QStringLiteral("ifnull(t_symbol,t_name)"), m_fillWhereClause);
        SKGServices::SKGUnitInfo primary = m_document->getPrimaryUnit();
        if (!primary.Symbol.isEmpty()) {
            this->setCurrentIndex(this->findText(primary.Symbol));
        }
    }
}

void SKGUnitComboBox::dataModified(const QString& iTableName, int iIdTransaction)
{
    Q_UNUSED(iIdTransaction)

    // Refresh widgets
    if (m_document != nullptr) {
        QSqlDatabase* db = m_document->getMainDatabase();
        setEnabled(db != nullptr);
        if (db != nullptr && (iTableName == QStringLiteral("unit") || iTableName.isEmpty())) {
            refershList();
        }
    }
}


