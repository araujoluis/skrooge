/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGADDOPERATION_H
#define SKGADDOPERATION_H

#include <krunner/abstractrunner.h>

/**
 * @brief An KRunner addon to create a new operation
 *
 */
class SKGAddOperation : public Plasma::AbstractRunner
{
    Q_OBJECT

public:
    /**
     * @brief The constructor
     *
     * @param iParent The parent object
     * @param args The list of arguments
     */
    SKGAddOperation(QObject* iParent, const QVariantList& args);

    /**
     * @brief Check if the user input match
     *
     * @param iContext The KRunner context
     * @return void
     */
    void match(Plasma::RunnerContext& iContext) override;

    /**
     * @brief Execute the creation of operation
     *
     * @param iContext The KRunner context
     * @param iMatch The query match
     * @return void
     */
    void run(const Plasma::RunnerContext& iContext, const Plasma::QueryMatch& iMatch) override;

    /**
     * @brief Reload the configuration
     *
     * @return void
     */
    void reloadConfiguration() override;

protected Q_SLOTS:
    void init() override;

private:
    QString m_triggerWord;
};

#endif
