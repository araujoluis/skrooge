/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGBUDGETDELEGATE_H
#define SKGBUDGETDELEGATE_H
/** @file
* This file is a delegate for budget.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include <qstyleditemdelegate.h>

class SKGDocument;

/**
 * This file is a delegate for budget
 */
class SKGBudgetDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    explicit SKGBudgetDelegate(QObject* iParent, SKGDocument* iDoc);

    /**
     * Default Destructor
     */
    ~SKGBudgetDelegate() override;


    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;


private:
    Q_DISABLE_COPY(SKGBudgetDelegate)

    SKGDocument* m_document;
    QString m_negativeStyleSheet;
    QString m_neutralStyleSheet;
    QString m_positiveStyleSheet;
};

#endif  // SKGBUDGETDELEGATE_H
