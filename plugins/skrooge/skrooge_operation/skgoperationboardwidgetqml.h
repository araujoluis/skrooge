/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGOPERATIONBOARDWIDGETQML_H
#define SKGOPERATIONBOARDWIDGETQML_H
/** @file
* This file is Skrooge plugin for operation management.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include "skghtmlboardwidget.h"

class QAction;
class SKGPeriodEdit;

/**
 * This file is Skrooge plugin for operation management
 */
class SKGOperationBoardWidgetQml : public SKGHtmlBoardWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGOperationBoardWidgetQml(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGOperationBoardWidgetQml() override;

    /**
     * Get the current state
     * MUST BE OVERWRITTEN
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    QString getState() override;

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    void setState(const QString& iState) override;

private Q_SLOTS:
    void settingsModified();

private:
    Q_DISABLE_COPY(SKGOperationBoardWidgetQml)

    QAction* m_menuOpen;
    QAction* m_menuGroup;
    QAction* m_menuTransfer;
    QAction* m_menuTracked;
    QAction* m_menuSuboperation;

    SKGPeriodEdit* m_periodEdit1;
    SKGPeriodEdit* m_periodEdit2;
};

#endif  // SKGOPERATIONBOARDWIDGETQML_H
