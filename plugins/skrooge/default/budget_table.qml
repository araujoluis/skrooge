/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

RowLayout {
    id: grid
    property var m: report==null ? null : report.budget_table
    property var pixel_size: report==null ? 0 : report.point_size
    spacing: 2

    ColumnLayout {
        spacing: 0

        // Set titles
        Repeater {
            model: m
            Label {
                Layout.fillWidth: true
                font.bold: index == 0 || modelData[0]
                font.pixelSize: pixel_size
                text: modelData[1]
                horizontalAlignment: index == 0 ? Text.AlignHCenter : Text.AlignLeft
            }
        }
    }

    Repeater {
        model: [2, 3, 4]
        ColumnLayout {
            spacing: 0
            property var modelId: modelData

            // Set values
            Repeater {
                model: m
                SKGValue {
                    font.pixelSize: pixel_size
                    Layout.fillWidth: true
                    horizontalAlignment: index == 0 ? Text.AlignHCenter : Text.AlignRight
                    font.bold: index == 0 || modelData[0]

                    value: index == 0  ? null : modelData[parent.modelId]
                    backgroundColor: '#' + (value == null || value < 0 ? color_negativetext : color_positivetext)
                    text: index == 0 ? modelData[parent.modelId] : parent.modelId!=2 ? document.formatPrimaryMoney(modelData[parent.modelId]) : document.formatPrimaryMoney(modelData[5])

                    Timer {
                        interval: 2000; running: index != 0 && parent.modelId==2 && document.formatPrimaryMoney(modelData[5])!=document.formatPrimaryMoney(modelData[2]); repeat: true
                        onTriggered: {

                            if(parent.text == document.formatPrimaryMoney(modelData[5])) {
                                parent.text = document.formatPrimaryMoney(modelData[2])
                                parent.font.strikeout = true
                            }else {
                                parent.text = document.formatPrimaryMoney(modelData[5])
                                parent.font.strikeout = false
                            }
                        }
                    }
                }
            }
        }
    }
}
