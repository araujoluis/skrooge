/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
import QtQuick 2.0
import QtQuick.Controls 1.0

Row {
    spacing: 5
    property var pixel_size: report==null ? 0 : report.point_size

    Rectangle {
        id: rect1
        width: 50
        height: width
        color: "#" + (report==null ? "red" : report.personal_finance_score_details.color)
        radius: width / 10

        Label {
            id: pfstext
            text: report==null ? null : report.personal_finance_score_details.value.toFixed(2)
            fontSizeMode: Text.Fit
            minimumPixelSize: 10
            font.pixelSize: 72
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: rect1
        }
    }

    Column {
        spacing: 2
        Label {
            id: t1
            width: rect1.width * 4
            height: rect1.height / 2
            text: '=' + title_networth + ' / ' + title_annual_spending
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
        }
        Label {
            id: t2
            width: t1.width
            height: t1.height
            text: '=' + document.formatPrimaryMoney(report==null ? 0 : report.networth) + ' / ' + document.formatPrimaryMoney(report==null ? 0 : report.annual_spending)
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
        }
        Label {
            id: msg
            width: t1.width
            height: t1.height
            text: report==null ? null : report.personal_finance_score_details.message
            fontSizeMode: Text.Fit
            minimumPixelSize: 10
            font.pixelSize: 72
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
        }
    }
}
