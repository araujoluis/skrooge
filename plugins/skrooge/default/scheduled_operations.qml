/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

ColumnLayout {
    id: grid
    property var m: report==null ? null : report.scheduled_operations
    property var pixel_size: report==null ? 0 : report.point_size
    spacing: 0

    Repeater {
	model: m
        Row {
            SKGValue {
                font.pixelSize: pixel_size
                id: l
                text: modelData[1]
                url: modelData[2]!="" ? "skg://skrooge_scheduled_plugin/?selection="+modelData[2] : ""
                bold: modelData[0]
            }
            Button {
                text: qsTr("Skip")
                anchors.top: l.top
                anchors.bottom: l.bottom                
                onClicked: {
                    panel.openPage("skg://skip_scheduled_operations/?selection="+modelData[2])
                }
                visible: modelData[2]!=""
            }
        }
    }
}
