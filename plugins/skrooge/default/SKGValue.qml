/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Label {
    property var value: null
    property var max: null
    property var bold: false
    property var url: ""
    property var backgroundColor: "#FF0000"
    property var point_size: 10
    
    color: '#' + (value == null || max != null ? color_normaltext : (value < 0 ? color_negativetext : color_positivetext))
    
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
    font.pixelSize: pixel_size
    font.bold: bold

    MouseArea {
        anchors.fill: parent
        cursorShape: url.length ? Qt.PointingHandCursor : Qt.ArrowCursor
        onClicked: {
            if (url.length) panel.openPage(url)
        }
    }
        
    Rectangle {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        color: parent.backgroundColor
        width: parent.max == null || parent.max == 0 ? 0 : Math.abs(parent.width * parent.value / parent.max)
        z: -1
        visible: parent.value != null && parent.max != null
        
        Behavior on width {
            NumberAnimation {
                duration: 300
                easing.type: Easing.InOutQuad
            }
        }
    }
}
