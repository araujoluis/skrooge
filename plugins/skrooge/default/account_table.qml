/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

RowLayout {
    id: grid
    property var m: report==null ? null : report.account_table
    property var pixel_size: report==null ? 0 : report.point_size
    spacing: 2

    function maxValues(m, id) {
        var output = 0
        for (var i = 1; i < m.length; i++) {
            if (!m[i][0] && Math.abs(m[i][id]) > output)
                output = Math.abs(m[i][id])
        }
        return output
    }

    ColumnLayout {
        spacing: 0

        // Set titles
        Repeater {
            model: m
            Label {
                Layout.fillWidth: true
                font.bold: index == 0 || modelData[0]
                font.pixelSize: pixel_size
                text: modelData[1]
                horizontalAlignment: index == 0 ? Text.AlignHCenter : Text.AlignLeft
            }
        }
    }

    Repeater {
        model: 6
        ColumnLayout {
            spacing: 0
            property var modelId: modelData + 2

            // Set values
            Repeater {
                model: m
                SKGValue {
                    font.pixelSize: pixel_size
                    Layout.fillWidth: true
                    horizontalAlignment: index == 0 ? Text.AlignHCenter : Text.AlignRight
                    font.bold: index == 0 || modelData[0]

                    value: index == 0 || parent.modelId == 4 || parent.modelId == 7 ? null : modelData[parent.modelId]
                    max: parent.modelId == 4 || parent.modelId == 7 || modelData[0] ? null : maxValues(m, parent.modelId)
                    backgroundColor: '#' + (value == null || value < 0 ? color_negativetext : color_positivetext)
                    text: index == 0 ? modelData[parent.modelId] : parent.modelId == 4 || parent.modelId == 7 ? document.formatPercentage(modelData[parent.modelId]) : document.formatPrimaryMoney(modelData[parent.modelId])
		    url: font.bold || parent.modelId != 6 ? "" : "skg://Skrooge_operation_plugin/?operationWhereClause=t_ACCOUNT='"+ modelData[1] + "'&title="+ modelData[1] + "&title_icon=view-bank-account"
                }
            }
        }
    }
}
