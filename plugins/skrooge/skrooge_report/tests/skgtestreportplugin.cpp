/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test for SKGReportPlugin component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestreportplugin.h"
#include "skgdocumentbank.h"
#include "../skgreportplugin.h"
#include "../../../../tests/skgbasemodelertest/skgtestmacro.h"

#include <QAction>

void SKGTESTReportPlugin::TestPlugin()
{
    SKGDocumentBank doc;
    SKGReportPlugin plugin(nullptr, nullptr, QVariantList());
    SKGTESTPLUGIN(plugin, doc);
    QCOMPARE(plugin.isInPagesChooser(), true);
    QCOMPARE(plugin.isEnabled(), true);

    SKGTESTTRIGGERACTION("open_report");
    SKGTESTTRIGGERACTION("view_open_very_old_operations");
    SKGTESTTRIGGERACTION("view_open_very_far_operations");
}

QTEST_MAIN(SKGTESTReportPlugin)

