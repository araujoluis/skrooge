/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGPROPERTIESPLUGIN_H
#define SKGPROPERTIESPLUGIN_H
/** @file
 * A plugin to manage properties on objects.
*
* @author Stephane MANKOWSKI
 */
#include <qprocess.h>

#include "skginterfaceplugin.h"

class SKGPropertiesPluginDockWidget;
class QMenu;
class QDockWidget;

/**
 * A plugin to manage properties on objects
 */
class SKGPropertiesPlugin : public SKGInterfacePlugin
{
    Q_OBJECT
    Q_INTERFACES(SKGInterfacePlugin)

public:
    /**
     * Default Constructor
     */
    explicit SKGPropertiesPlugin(QWidget* iWidget, QObject* iParent, const QVariantList& iArg);

    /**
     * Default Destructor
     */
    ~SKGPropertiesPlugin() override;

    /**
     * Called to initialise the plugin
     * @param iDocument the main document
     * @return true if the plugin is compatible with the document
     */
    bool setupActions(SKGDocument* iDocument) override;

    /**
     * Must be modified to refresh widgets after a modification.
     */
    void refresh() override;

    /**
     * The dock widget of the plugin.
     * @return The dock widget of the plugin
     */
    QDockWidget* getDockWidget() override;

    /**
     * The icon of the plugin.
     * @return The icon of the plugin
     */
    QString icon() const override;

    /**
     * The title of the plugin.
     * @return The title of the plugin
     */
    QString title() const override;

    /**
     * The tips list of the plugin.
     * @return The tips list of the plugin
     */
    QStringList tips() const override;

    /**
     * Must be implemented to set the position of the plugin.
     * @return integer value between 0 and 999 (default = 999)
     */
    int getOrder() const override;

private Q_SLOTS:
    void onAddProperty();
    void onDownloadAndAddBills();
    void onShowAddPropertyMenu();
    void onBillsRetreived();

private:
    Q_DISABLE_COPY(SKGPropertiesPlugin)

    QProcess m_billsProcess;
    QStringList m_bills;

    SKGDocument* m_currentDocument;
    QDockWidget* m_dockWidget;
    SKGPropertiesPluginDockWidget* m_dockContent;
    QMenu* m_addPropertyMenu;
};

#endif  // SKGPROPERTIESPLUGIN_H
