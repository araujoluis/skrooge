/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGPROPERTIESPLUGINDOCKWIDGET_H
#define SKGPROPERTIESPLUGINDOCKWIDGET_H
/** @file
 * A plugin to manage properties on objects
*
* @author Stephane MANKOWSKI
*/
#include "skgwidget.h"
#include "ui_skgpropertiesplugindockwidget_base.h"

/**
 * A plugin to manage properties on objects
 */
class SKGPropertiesPluginDockWidget : public SKGWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGPropertiesPluginDockWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGPropertiesPluginDockWidget() override;

    /**
     * Get the main widget
     * @return a widget
     */
    QWidget* mainWidget() override;

public Q_SLOTS:
    /**
    * Refresh the content.
     */
    virtual void refresh();

private Q_SLOTS:
    void onSelectionChanged();
    void onAddProperty();
    void onRenameProperty();
    void onRemoveProperty();
    void onSelectFile();
    void onOpenFile();
    void onOpenPropertyFileByUrl();
    void cleanEditor();

private:
    Q_DISABLE_COPY(SKGPropertiesPluginDockWidget)

    void openPropertyFile(const SKGPropertyObject& iProp);

    Ui::skgpropertiesdockplugin_base ui{};
};

#endif  // SKGPROPERTIESPLUGINDOCKWIDGET_H
