/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGTIPOFDAYBOARDWIDGET_H
#define SKGTIPOFDAYBOARDWIDGET_H
/** @file
* This file is a plugin for tip of day.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include "skgboardwidget.h"
#include "ui_skgtipofdayboardwidget.h"

/**
 * This file is a plugin for tip of day
 */
class SKGTipOfDayBoardWidget : public SKGBoardWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGTipOfDayBoardWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGTipOfDayBoardWidget() override;

private Q_SLOTS:
    void onModified();

private:
    Q_DISABLE_COPY(SKGTipOfDayBoardWidget)

    Ui::skgtipofdayboardwidget ui{};
};

#endif  // SKGTIPOFDAYBOARDWIDGET_H
