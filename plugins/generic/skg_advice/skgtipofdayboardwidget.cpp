/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is plugin for tip of day.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtipofdayboardwidget.h"

#include <qfileinfo.h>

#include <klocalizedstring.h>
#include <kcolorscheme.h>

#include "skgmainpanel.h"
#include "skgservices.h"
#include "skgtraces.h"

SKGTipOfDayBoardWidget::SKGTipOfDayBoardWidget(QWidget* iParent, SKGDocument* iDocument)
    : SKGBoardWidget(iParent, iDocument, i18nc("Dashboard widget title", "Tip of the day"))
{
    SKGTRACEINFUNC(10)

    auto f = new QFrame();
    ui.setupUi(f);
    setMainWidget(f);
    ui.kIcon->setIcon(SKGServices::fromTheme(QStringLiteral("ktip")));

    onModified();

    connect(ui.kIcon, &QPushButton::clicked, this, &SKGTipOfDayBoardWidget::onModified);
    connect(ui.kText, &QLabel::linkActivated, this, [ = ](const QString & val) {
        SKGMainPanel::getMainPanel()->openPage(val);
    });

    // Refresh
    connect(getDocument(), &SKGDocument::transactionSuccessfullyEnded, this, &SKGTipOfDayBoardWidget::onModified, Qt::QueuedConnection);
}

SKGTipOfDayBoardWidget::~SKGTipOfDayBoardWidget()
{
    SKGTRACEINFUNC(10)
}

void SKGTipOfDayBoardWidget::onModified()
{
    auto text = SKGMainPanel::getMainPanel()->getTipOfDay();

    // Remove color of hyperlinks
    KColorScheme scheme(QPalette::Normal, KColorScheme::Window);
    auto color = scheme.foreground(KColorScheme::NormalText).color().name().right(6);
    text = text.replace(QStringLiteral("<a href"), QStringLiteral("<a style=\"color: #") + color + ";\" href");

    ui.kText->setText(text);
}
