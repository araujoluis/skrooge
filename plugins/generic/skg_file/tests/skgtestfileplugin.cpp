/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test for SKGFilePlugin component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestfileplugin.h"
#include "skgdocument.h"
#include "../skgfileplugin.h"
#include "../../../../tests/skgbasemodelertest/skgtestmacro.h"

#include <QAction>

void SKGTESTFilePlugin::TestPlugin()
{
    SKGDocument doc;
    SKGFilePlugin plugin(nullptr, nullptr, QVariantList());
    SKGTESTPLUGIN(plugin, doc);
    QCOMPARE(plugin.isInPagesChooser(), false);
    QCOMPARE(plugin.isEnabled(), true);

    SKGTESTTRIGGERACTION("file_new");
    SKGTESTTRIGGERACTION("file_open");
    SKGTESTTRIGGERACTION("file_save");
    SKGTESTTRIGGERACTION("file_save_as");
    SKGTESTTRIGGERACTION("file_change_password");
}

QTEST_MAIN(SKGTESTFilePlugin)
