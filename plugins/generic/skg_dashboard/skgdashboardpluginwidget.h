/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGDASHBOARDPLUGINWIDGET_H
#define SKGDASHBOARDPLUGINWIDGET_H
/** @file
 * A dashboard
 *
 * @author Stephane MANKOWSKI
 */
#include <qlist.h>
#include <qstringlist.h>
#include <qtimer.h>

#include "skgtabpage.h"
#include "ui_skgdashboardpluginwidget_base.h"

class QMenu;
class SKGInterfacePlugin;
class SKGFlowLayout;
class SKGBoardWidget;

/**
 * A dashboard
 */
class SKGDashboardPluginWidget : public SKGTabPage
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGDashboardPluginWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGDashboardPluginWidget() override;

    /**
     * Get the current state
     * MUST BE OVERWRITTEN
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    QString getState() override;

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    void setState(const QString& iState) override;

    /**
     * Get attribute name to save the default state
     * MUST BE OVERWRITTEN
     * @return attribute name to save the default state.
     */
    QString getDefaultStateAttribute() override;

    /**
     * Get the zoomable widget.
     * The default implementation returns the main widget.
     * @return the zoomable widget.
     */
    QWidget* zoomableWidget() override;

    /**
     * Get the printable widgets.
     * The default implementation returns the main widget.
     * @return the printable widgets.
     */
    QList<QWidget*> printableWidgets() override;

protected:
    /**
     * Event filtering
     * @param iObject object
     * @param iEvent event
     * @return In your reimplementation of this function, if you want to filter the event out, i.e. stop it being handled further, return true; otherwise return false.
     */
    bool eventFilter(QObject* iObject, QEvent* iEvent) override;

public Q_SLOTS:
    /**
    * Refresh the content.
     */
    virtual void refresh();

Q_SIGNALS:
    /**
     * When an applet id added
     */
    void appletAdded(const QString& /*_t1*/);

private Q_SLOTS:
    void showHeaderMenu(QPoint iPos);
    void onAddWidget();
    void onRemoveWidget();
    void onMoveWidget(int iMove);

private:
    Q_DISABLE_COPY(SKGDashboardPluginWidget)

    void addItem(SKGInterfacePlugin* iDashboard, int iIndex, int iZoom = -10, const QString& iState = QString());
    void moveItem(int iFrom, int iTo);
    static SKGBoardWidget* parentBoardWidget(QWidget* iWidget);

    Ui::skgdashboardplugin_base ui{};
    SKGFlowLayout* m_flowLayout;

    QStringList m_items;
    QList<SKGBoardWidget*> m_itemsPointers;
    QMenu* m_menu;
    QMenu* m_addMenu;
    QTimer m_timer;

    QPoint m_clickedPoint;
    QPoint m_lastPoint;
};

#endif  // SKGDASHBOARDPLUGINWIDGET_H
