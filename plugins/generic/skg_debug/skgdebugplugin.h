/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGDEBUGPLUGIN_H
#define SKGDEBUGPLUGIN_H
/** @file
* This file is a plugin for debug.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skginterfaceplugin.h"


/**
 * This file is a plugin for debug
 */
class SKGDebugPlugin : public SKGInterfacePlugin
{
    Q_OBJECT
    Q_INTERFACES(SKGInterfacePlugin)

public:
    /**
     * Default Constructor
     */
    explicit SKGDebugPlugin(QWidget* iWidget, QObject* iParent, const QVariantList& iArg);

    /**
     * Default Destructor
     */
    ~SKGDebugPlugin() override;

    /**
     * Called to initialise the plugin
     * @param iDocument the main document
     * @return true if the plugin is compatible with the document
     */
    bool setupActions(SKGDocument* iDocument) override;

    /**
     * The page widget of the plugin.
     * @return The page widget of the plugin
     */
    SKGTabPage* getWidget() override;

    /**
     * The title of the plugin.
     * @return The title of the plugin
     */
    QString title() const override;

    /**
     * The icon of the plugin.
     * @return The icon of the plugin
     */
    QString icon() const override;

    /**
     * The toolTip of the plugin.
     * @return The toolTip of the plugin
     */
    QString toolTip() const override;

    /**
     * Must be implemented to know if a plugin must be display in pages chooser.
     * @return true of false (default = false)
     */
    bool isInPagesChooser() const override;

    /**
     * Must be implemented to know if this plugin is enabled
     * @return true of false (default = true)
     */
    bool isEnabled() const override;

private Q_SLOTS:
    void onRestartProfiling();
    void onOpenProfiling();

private:
    Q_DISABLE_COPY(SKGDebugPlugin)

    SKGDocument* m_currentDocument;
};

#endif  // SKGDEBUGPLUGIN_H
