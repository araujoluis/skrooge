/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A plugin to select all
 *
 * @author Stephane MANKOWSKI
 */
#include "skgselectallplugin.h"

#include <kaboutdata.h>
#include <kactioncollection.h>
#include <klocalizedstring.h>
#include <kpluginfactory.h>
#include <kstandardaction.h>

#include <qabstractitemview.h>

#include "skgmainpanel.h"
#include "skgtraces.h"
#include "skgtreeview.h"
/**
 * This plugin factory.
 */
K_PLUGIN_FACTORY(SKGSelectAllPluginFactory, registerPlugin<SKGSelectAllPlugin>();)

SKGSelectAllPlugin::SKGSelectAllPlugin(QWidget* iWidget, QObject* iParent, const QVariantList& /*iArg*/) :
    SKGInterfacePlugin(iParent), m_currentDocument(nullptr), m_selectionMessage(nullptr)
{
    Q_UNUSED(iWidget)
    SKGTRACEINFUNC(10)

    connect(SKGMainPanel::getMainPanel(), &SKGMainPanel::selectionChanged, this, &SKGSelectAllPlugin::onSelectionChanged);
}

SKGSelectAllPlugin::~SKGSelectAllPlugin()
{
    SKGTRACEINFUNC(10)
    m_currentDocument = nullptr;
    m_selectionMessage = nullptr;
}

bool SKGSelectAllPlugin::setupActions(SKGDocument* iDocument)
{
    SKGTRACEINFUNC(10)

    m_currentDocument = iDocument;

    setComponentName(QStringLiteral("skg_selectall"), title());
    setXMLFile(QStringLiteral("skg_selectall.rc"));

    // Menu
    registerGlobalAction(QStringLiteral("edit_select_all"), actionCollection()->addAction(KStandardAction::SelectAll, QStringLiteral("edit_select_all"), this, SLOT(onSelectAll())), QStringList(), 0);
    return true;
}

QString SKGSelectAllPlugin::title() const
{
    return i18nc("Select all objects in a list", "Select all");
}

QString SKGSelectAllPlugin::icon() const
{
    return QStringLiteral("edit-select-all");
}

QString SKGSelectAllPlugin::toolTip() const
{
    return i18nc("Select all objects in a list", "Select all");
}

int SKGSelectAllPlugin::getOrder() const
{
    return 6;
}

QStringList SKGSelectAllPlugin::tips() const
{
    QStringList output;
    return output;
}

void SKGSelectAllPlugin::onSelectionChanged()
{
    SKGTRACEINFUNC(10)
    if (SKGMainPanel::getMainPanel() != nullptr) {
        if (m_selectionMessage == nullptr) {
            // Add statusbar
            m_selectionMessage = new QLabel(SKGMainPanel::getMainPanel());
            SKGMainPanel::getMainPanel()->statusBar()->insertPermanentWidget(1, m_selectionMessage);
        }
        SKGObjectBase::SKGListSKGObjectBase selection = SKGMainPanel::getMainPanel()->getSelectedObjects();
        int nb = selection.count();
        if (nb != 0) {
            double sum = 0.0;
            double max = -std::numeric_limits<double>::max();
            double min = std::numeric_limits<double>::max();
            int count = 0;
            for (int i = 0; i < nb; ++i) {
                const SKGObjectBase& obj(selection.at(i));
                QString val = obj.getAttribute(QStringLiteral("f_REALCURRENTAMOUNT"));
                if (val.isEmpty()) {
                    val = obj.getAttribute(QStringLiteral("f_CURRENTAMOUNT"));
                }
                if (!val.isEmpty()) {
                    double vald = SKGServices::stringToDouble(val);
                    sum += vald;
                    max = qMax(max, vald);
                    min = qMin(min, vald);
                    count++;
                }
            }
            if (count > 1) {
                m_selectionMessage->setText(i18n("Selection: %1 lines for %2 (min: %3, average: %4, max: %5)",
                                                 nb,
                                                 m_currentDocument->formatPrimaryMoney(sum),
                                                 m_currentDocument->formatPrimaryMoney(min),
                                                 m_currentDocument->formatPrimaryMoney(sum / count),
                                                 m_currentDocument->formatPrimaryMoney(max)));
            } else if (count > 0) {
                m_selectionMessage->setText(i18n("Selection: %1 line for %2", nb, m_currentDocument->formatPrimaryMoney(sum)));
            } else {
                m_selectionMessage->setText(i18np("Selection: %1 line", "Selection: %1 lines", nb));
            }
        } else {
            m_selectionMessage->clear();
        }
    }
}

void SKGSelectAllPlugin::onSelectAll()
{
    SKGTRACEINFUNC(10)
    if (SKGMainPanel::getMainPanel() != nullptr) {
        SKGTabPage* page = SKGMainPanel::getMainPanel()->currentPage();
        if (page != nullptr) {
            auto* view = qobject_cast<QAbstractItemView*>(page->mainWidget());
            if (view != nullptr) {
                view->selectAll();
            }
        }
    }
}

#include <skgselectallplugin.moc>
