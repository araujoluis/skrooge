/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGUNDOREDOPLUGINDOCKWIDGET_H
#define SKGUNDOREDOPLUGINDOCKWIDGET_H
/** @file
* This file is a undoredo for bank management.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include "skgwidget.h"
#include "ui_skgundoredoplugindockwidget_base.h"

/**
 * This file is a plugin for undoredo management
 */
class SKGUndoRedoPluginDockWidget : public SKGWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGUndoRedoPluginDockWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGUndoRedoPluginDockWidget() override;

    /**
     * Get the main widget
     * @return a widget
     */
    QWidget* mainWidget() override;

private Q_SLOTS:
    void onUndoRedo(const QModelIndex& index);
    void onClearHistory();

private:
    Q_DISABLE_COPY(SKGUndoRedoPluginDockWidget)

    Ui::skgundoredoplugindockwidget_base ui{};
};

#endif  // SKGUNDOREDOPLUGINDOCKWIDGET_H
