/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * The grantlee's filter to get attribute of an object.
 *
 * @author Stephane MANKOWSKI
 */
#include "skgobjectfilter.h"
#include "skgobjectbase.h"
#include "skgtraces.h"

#include "grantlee/util.h"

QVariant SKGObjectFilter::doFilter(const QVariant& input, const QVariant& argument, bool autoescape) const
{
    Q_UNUSED(autoescape)
    SKGObjectBase obj = input.value<SKGObjectBase>();
    return obj.getAttribute(Grantlee::getSafeString(argument));
}

bool SKGObjectFilter::isSafe() const
{
    return true;
}
