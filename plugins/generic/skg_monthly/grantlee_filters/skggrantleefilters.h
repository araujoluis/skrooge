/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGGRANTLEEFILTERS_H
#define SKGGRANTLEEFILTERS_H
/** @file
 * The grantlee's plugin to define filters.
 *
 * @author Stephane MANKOWSKI
 */
#include <grantlee/taglibraryinterface.h>
#include <qobject.h>

/**
 * The grantlee's plugin to define filters
 */
class SKGGrantleeFilters : public QObject, public Grantlee::TagLibraryInterface
{
    Q_OBJECT
    Q_INTERFACES(Grantlee::TagLibraryInterface)
    Q_PLUGIN_METADATA(IID "org.grantlee.TagLibraryInterface")

public:
    /**
     * Default Constructor
     */
    explicit SKGGrantleeFilters(QObject* iParent = nullptr);

    /**
     * Default Destructor
     */
    ~SKGGrantleeFilters() override;

    /**
     * Returns the Filter implementations available in this library
     * @param iName the name
     * @return the implementations
     */
    QHash<QString, Grantlee::Filter*> filters(const QString& iName = QString()) override;
};

#endif  // SKGGRANTLEEFILTERS_H
