/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGOBJECTFILTER_H
#define SKGOBJECTFILTER_H
/** @file
 * The grantlee's filter to get attribute of an object.
 *
 * @author Stephane MANKOWSKI
 */
#include <grantlee/filter.h>
#include <qobject.h>

/**
 * The grantlee's filter to get attribute of an object
 */
class SKGObjectFilter : public Grantlee::Filter
{
public:
    /**
     * Do the filtering
     * @param input the input
     * @param argument the argument
     * @param autoescape the autoescape mode
     * @return the filtered value
     */
    QVariant doFilter(const QVariant& input, const QVariant& argument = QVariant(), bool autoescape = false) const override;

    /**
     * To know if the filter is safe
     * @return true or false
     */
    bool isSafe() const override;
};

#endif  // SKGOBJECTFILTER_H
