/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * The grantlee's plugin to define filters.
 *
 * @author Stephane MANKOWSKI
 */
#include "skggrantleefilters.h"



#include "skgdocumentfilter.h"
#include "skgobjectfilter.h"

SKGGrantleeFilters::SKGGrantleeFilters(QObject* iParent): QObject(iParent)
{}

SKGGrantleeFilters::~SKGGrantleeFilters()
    = default;

QHash< QString, Grantlee::Filter* > SKGGrantleeFilters::filters(const QString& iName)
{
    Q_UNUSED(iName)

    QHash<QString, Grantlee::Filter*> filtersList;
    filtersList.insert(QStringLiteral("query"), new SKGDocumentQueryFilter());
    filtersList.insert(QStringLiteral("table"), new SKGDocumentTableFilter());
    filtersList.insert(QStringLiteral("display"), new SKGDocumentDisplayFilter());
    filtersList.insert(QStringLiteral("att"), new SKGObjectFilter());
    filtersList.insert(QStringLiteral("money"), new SKGMoneyFilter());
    filtersList.insert(QStringLiteral("percent"), new SKGPercentFilter());
    filtersList.insert(QStringLiteral("filesizeformat"), new SKGFileSizeFilter());
    filtersList.insert(QStringLiteral("dump"), new SKGDumpFilter());
    filtersList.insert(QStringLiteral("encode"), new SKGUrlEncodeFilter());
    filtersList.insert(QStringLiteral("replace"), new SKGReplaceFilter());
    return filtersList;
}
