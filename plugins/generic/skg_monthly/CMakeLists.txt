#***************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program. If not, see <https://www.gnu.org/licenses/>  *
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_MONTHLY ::..")

PROJECT(plugin_monthly)

IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)

ADD_SUBDIRECTORY(grantlee_filters)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_monthly_SRCS
	skgmonthlyplugin.cpp
	skgmonthlypluginwidget.cpp)

ki18n_wrap_ui(skg_monthly_SRCS skgmonthlypluginwidget_base.ui)

ADD_LIBRARY(skg_monthly MODULE ${skg_monthly_SRCS})
TARGET_LINK_LIBRARIES(skg_monthly KF5::Parts KF5::ItemViews KF5::NewStuff KF5::Archive Qt5::PrintSupport skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(TARGETS skg_monthly DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skg-plugin-monthly.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_monthly.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_monthly )
