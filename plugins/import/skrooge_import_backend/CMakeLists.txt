#***************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program. If not, see <https://www.gnu.org/licenses/>  *
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_BACKEND ::..")

PROJECT(plugin_import_backend)

ADD_SUBDIRECTORY(backends)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_backend_SRCS
	skgimportpluginbackend.cpp
)

ADD_LIBRARY(skrooge_import_backend MODULE ${skrooge_import_backend_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_backend KF5::Parts Qt5::Concurrent skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_backend DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-backend.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-backend-type.desktop DESTINATION ${KDE_INSTALL_KSERVICETYPES5DIR})
INSTALL(PROGRAMS skrooge-sabb.py DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
