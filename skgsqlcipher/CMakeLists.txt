#***************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program. If not, see <https://www.gnu.org/licenses/>  *
#***************************************************************************
MESSAGE( STATUS "..:: SKGSQLCIPHER ::..")

PROJECT(skgsqlcipher)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skgsqlcipher_sources skgsqlcipherdriverplugin.cpp qsql_sqlite.cpp)

ADD_LIBRARY(libskgsqlcipher MODULE ${skgsqlcipher_sources})
TARGET_INCLUDE_DIRECTORIES(libskgsqlcipher PRIVATE ${Qt5Sql_PRIVATE_INCLUDE_DIRS} ${SQLCIPHER_INCLUDE_DIRS})
TARGET_LINK_LIBRARIES(libskgsqlcipher Qt5::Sql ${SQLCIPHER_LIBRARIES})

GENERATE_EXPORT_HEADER(libskgsqlcipher BASE_NAME skgsqlcipher)

########### install files ###############
INSTALL(TARGETS libskgsqlcipher DESTINATION ${KDE_INSTALL_QTPLUGINDIR}/sqldrivers)    

