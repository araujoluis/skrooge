/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test for SKGMainPanel component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestmainpanel.h"
#include "skgboardwidget.h"
#include "skgdocumentbank.h"
#include "skgmainpanel.h"
#include "skgtestmacro.h"
#include "skgtraces.h"

void SKGTESTMainPanel::Test()
{
    // Initialize document
    SKGDocumentBank doc;

    // Create main panel
    SKGMainPanel mainpanel(nullptr, &doc);

    QVERIFY2(!doc.load(SKGTest::getTestPath(QStringLiteral("IN")) % "/advice.skg"), "Load document failed");
    SKGError err;
    {
        // Scope of the transaction
        SKGBEGINTRANSACTION(doc, QStringLiteral("PROP"), err)
        doc.setParameter(QStringLiteral("SKG_LAST_BUDGET_PROCESSING"), QLatin1String(""));
    }

    // Check plugins
    {
        SKGBEGINTRANSACTION(doc, QStringLiteral("PLUGINS"), err)
        int i = 0;
        SKGInterfacePlugin* plugin = nullptr;
        do {
            plugin = mainpanel.getPluginByIndex(i);
            if (plugin != nullptr) {
                SKGTRACE << i << ": " << plugin->title() << "(" << plugin->icon() << ")" << endl;
                plugin->statusTip();
                plugin->toolTip();
                plugin->tips();
                plugin->getOrder();
                plugin->isInPagesChooser();
                plugin->isEnabled();

                SKGTabPage* tab = plugin->getWidget();
                if (tab != nullptr) {
                    tab->printableWidgets();
                    tab->activateEditor();
                    tab->getBookmarkID();
                    tab->setState(tab->getState());

                    if (tab->isZoomable()) {
                        tab->zoomableWidget();
                        tab->setZoomPosition(5);
                        QCOMPARE(tab->zoomPosition(), 5);
                    }
                }

                int nbd = plugin->getNbDashboardWidgets();
                for (int j = 0; j < nbd + 1; ++j) {
                    SKGTRACE << "    Dashboard " << j << "/" << nbd + 1 << ": " << plugin->getDashboardWidgetTitle(j) << endl;
                    SKGBoardWidget* bw = plugin->getDashboardWidget(j);
                    if (bw != nullptr) {
                        bw->setState(bw->getState());
                        bw->getDefaultStateAttribute();
                        bw->getFirstSelectedObject();
                    }
                }

                plugin->getDockWidget();
                plugin->getPreferenceWidget();
                plugin->getPreferenceSkeleton();
                plugin->savePreferences();

                SKGAdviceList adviceList = plugin->advice(QStringList());
                for (const auto& advice : qAsConst(adviceList)) {
                    SKGTRACE << "    Advice: " << advice.getUUID() << endl;
                    for (int k = 0; k < 5; ++k) {
                        plugin->executeAdviceCorrection(advice.getUUID(), k);
                    }
                }
            }
            ++i;
        } while (plugin != nullptr);

        // Compute advices
        SKGTRACE << "getAdvice" << endl;
        mainpanel.getAdvice();
    }

    // Functions
    SKGTRACE << "processArguments" << endl;
    mainpanel.processArguments(QStringList());

    mainpanel.openPage(QStringLiteral("skg://skrooge_operation_plugin"));
    SKGTabPage* page = mainpanel.currentPage();
    mainpanel.switchPinPage(page);
    mainpanel.currentPageHistoryItem();

    mainpanel.openPage(0, true);
    mainpanel.openPage(0, false);
    mainpanel.setCurrentPage(0);

    mainpanel.getNbSelectedObjects();

    mainpanel.countPages();
    mainpanel.page(0);
    mainpanel.currentPageIndex();

    mainpanel.closeAllOtherPages(page);
    mainpanel.closeAllPages(true);

    mainpanel.splashScreen();

    mainpanel.getTabWidget();

    mainpanel.setContextVisibility(1, true);
    mainpanel.setContextVisibility(1, false);
}

QTEST_MAIN(SKGTESTMainPanel)

