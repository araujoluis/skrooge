/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test for SKGTreeView component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtesttreeview.h"
#include "skgfilteredtableview.h"
#include "skgobjectmodel.h"
#include "skgsortfilterproxymodel.h"
#include "skgtestmacro.h"
#include "skgtreeview.h"

void SKGTESTTreeView::Test()
{
    // Initialize document
    SKGDocumentBank doc;
    QVERIFY2(!doc.load(SKGTest::getTestPath(QStringLiteral("OUT")) % "/advice.skg"), "Load document failed");

    // Initilialization view and model
    SKGFilteredTableView tableView(nullptr);
    auto m_objectModel = new SKGObjectModel(&doc, QStringLiteral("v_account_display"), QLatin1String(""), nullptr);
    tableView.setModel(m_objectModel);
    m_objectModel->setTable(QStringLiteral("v_operation_display_all"));
    QCOMPARE(m_objectModel->getGroupBy(), QLatin1String(""));
    m_objectModel->setGroupBy(QStringLiteral("d_date"));
    QCOMPARE(m_objectModel->getGroupBy(), QStringLiteral("d_date"));
    m_objectModel->setGroupBy(QStringLiteral("t_status"));
    m_objectModel->refresh();
    QTest::qWait(300);

    // SKGFilteredTableView
    tableView.setState(tableView.getState());

    QTest::qWait(2000);

    // SKGTreeView
    SKGTreeView* tree = tableView.getView();
    tree->exportInFile(SKGTest::getTestPath(QStringLiteral("OUT")) % "/skgtesttreeview/SKGTreeView.csv");
    tree->exportInFile(SKGTest::getTestPath(QStringLiteral("OUT")) % "/skgtesttreeview/SKGTreeView.txt");
    tree->exportInFile(SKGTest::getTestPath(QStringLiteral("OUT")) % "/skgtesttreeview/SKGTreeView.svg");
    tree->exportInFile(SKGTest::getTestPath(QStringLiteral("OUT")) % "/skgtesttreeview/SKGTreeView.pdf");
    tree->exportInFile(SKGTest::getTestPath(QStringLiteral("OUT")) % "/skgtesttreeview/SKGTreeView.html");
    tree->exportInFile(SKGTest::getTestPath(QStringLiteral("OUT")) % "/skgtesttreeview/SKGTreeView.odt");

    tree->getTable();
    tree->getFirstSelectedObject();

    tree->setTextResizable(true);
    QCOMPARE(tree->isTextResizable(), true);
    tree->setTextResizable(false);
    QCOMPARE(tree->isTextResizable(), false);

    tree->setZoomPosition(5);
    QCOMPARE(tree->zoomPosition(), 5);
    tree->setZoomPosition(6);
    QCOMPARE(tree->zoomPosition(), 6);

    tree->switchAutoResize();
    tree->setState(tree->getState());

    tree->getCurrentSchema();

    tableView.getSearchField()->setText(QStringLiteral("e +a -i"));
    tree->copy();

    for (int i = 0; i < 10; ++i) {
        tree->sortByColumn(i, Qt::AscendingOrder);
        QTest::qWait(1000);

        tree->sortByColumn(i, Qt::DescendingOrder);
        QTest::qWait(1000);
    }
}

QTEST_MAIN(SKGTESTTreeView)

