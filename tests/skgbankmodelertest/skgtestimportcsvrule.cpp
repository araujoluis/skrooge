/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestmacro.h"
#include "skgbankincludes.h"
#include "skgimportexportmanager.h"

/**
 * The main function of the import rule test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true)
    SKGError err;
    {
        // Test import SKGImportExportManager::CSV skrooge
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true) {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("IMPORT"), err) {
                SKGImportExportManager imp1(&document1, QUrl::fromLocalFile(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestimportcsvrule/notfound.csv"));
                QMap<QString, QString> parameters = imp1.getImportParameters();
                parameters[QStringLiteral("mode_csv_rule")] = 'Y';
                imp1.setImportParameters(parameters);
                SKGTESTERROR(QStringLiteral("imp1.importFile"), imp1.importFile(), false)  // FILE NOT FOUND
            }
            {
                SKGImportExportManager imp1(&document1, QUrl::fromLocalFile(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestimportcsvrule/test1.csv"));
                QMap<QString, QString> parameters = imp1.getImportParameters();
                parameters[QStringLiteral("mode_csv_rule")] = 'Y';
                imp1.setImportParameters(parameters);
                SKGTESTERROR(QStringLiteral("imp1.importFile"), imp1.importFile(), true)
            }
        }

        int nb = 0;
        SKGTESTERROR(QStringLiteral("imp1.getNbObjects"), document1.getNbObjects(QStringLiteral("rule"), QLatin1String(""), nb), true)
        SKGTEST(QStringLiteral("imp1.nb"), nb, 3)
    }

    // End test
    SKGENDTEST()
}
