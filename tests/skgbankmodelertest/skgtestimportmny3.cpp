/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestmacro.h"
#include "skgbankincludes.h"
#include "skgimportexportmanager.h"


struct test {
    QString fileName;
    QString password;
    QMap<QString, double> expectedAccountAmount;
};

/**
 * The main function of the unit test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true)

    {
        // Test import MNY
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true)
        SKGError err;
        {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("IMPORT_MNY"), err)
            SKGImportExportManager imp1(&document1, QUrl::fromLocalFile(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestimportmny1/A B/money2002.mny"));
            QMap<QString, QString> params = imp1.getImportParameters();
            params[QStringLiteral("install_sunriise")] = 'Y';
            imp1.setImportParameters(params);
            SKGTESTERROR(QStringLiteral("imp1.importFile"), imp1.importFile(), true)

            SKGAccountObject account(&document1);
            SKGTESTERROR(QStringLiteral("MNY.setName"), account.setName(QStringLiteral("None Investment")), true)
            SKGTESTERROR(QStringLiteral("MNY.load"), account.load(), true)
            SKGTEST(QStringLiteral("MNY.getCurrentAmount"), SKGServices::doubleToString(account.getCurrentAmount()), QStringLiteral("1.49"))
        }

        {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("IMPORT_MNY"), err)
            SKGImportExportManager imp1(&document1, QUrl::fromLocalFile(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestimportmny1/A B/money2002.mny"));
            QMap<QString, QString> params = imp1.getImportParameters();
            params[QStringLiteral("install_sunriise")] = 'Y';
            imp1.setImportParameters(params);
            SKGTESTERROR(QStringLiteral("imp1.importFile"), imp1.importFile(), true)

            SKGAccountObject account(&document1);
            SKGTESTERROR(QStringLiteral("MNY.setName"), account.setName(QStringLiteral("None Investment")), true)
            SKGTESTERROR(QStringLiteral("MNY.load"), account.load(), true)
            SKGTEST(QStringLiteral("MNY.getCurrentAmount"), SKGServices::doubleToString(account.getCurrentAmount()), QStringLiteral("1.49"))
        }
    }

    {
        // Test import MNY
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true)
        SKGError err;
        {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("IMPORT_MNY"), err)
            SKGImportExportManager imp1(&document1, QUrl::fromLocalFile(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestimportmny1/notreadable.mny"));
            QMap<QString, QString> params = imp1.getImportParameters();
            params[QStringLiteral("install_sunriise")] = 'Y';
            imp1.setImportParameters(params);
            SKGTEST(QStringLiteral("imp1.importFile"), imp1.importFile().getReturnCode(), ERR_READACCESS)
        }
    }

    {
        // Test import MNY
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true)
        SKGError err;
        {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("IMPORT_MNY"), err)
            SKGImportExportManager imp1(&document1, QUrl::fromLocalFile(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestimportmny3/transfer_and_mode.mny"));
            QMap<QString, QString> params = imp1.getImportParameters();
            params[QStringLiteral("install_sunriise")] = 'Y';
            imp1.setImportParameters(params);
            SKGTESTERROR(QStringLiteral("imp1.importFile"), imp1.importFile(), true)
        }

        bool test2 = false;
        SKGTESTERROR(QStringLiteral("MNY.existObjects"), document1.existObjects(QStringLiteral("operation"), QStringLiteral("t_comment='Mode should be ''text''' AND t_mode='text'"), test2), true)
        SKGTEST(QStringLiteral("MNY.existObjects-test"), static_cast<unsigned int>(test2), static_cast<unsigned int>(true))

        SKGTESTERROR(QStringLiteral("MNY.existObjects"), document1.existObjects(QStringLiteral("operation"), QStringLiteral("t_comment='Number should be 123456' AND t_number='123456'"), test2), true)
        SKGTEST(QStringLiteral("MNY.existObjects-test"), static_cast<unsigned int>(test2), static_cast<unsigned int>(true))

        SKGTESTERROR(QStringLiteral("MNY.existObjects"), document1.existObjects(QStringLiteral("operation"), QStringLiteral("t_comment='True transfert' AND i_group_id<>0"), test2), true)
        SKGTEST(QStringLiteral("MNY.existObjects-test"), static_cast<unsigned int>(test2), static_cast<unsigned int>(true))

        SKGTESTERROR(QStringLiteral("MNY.existObjects"), document1.existObjects(QStringLiteral("operation"), QStringLiteral("t_comment='Not a transfert, just same date/amount' AND i_group_id=0"), test2), true)
        SKGTEST(QStringLiteral("MNY.existObjects-test"), static_cast<unsigned int>(test2), static_cast<unsigned int>(true))
    }

    // End test
    SKGENDTEST()
}
