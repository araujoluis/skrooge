/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <qfile.h>

#include "skgdocumentbank.h"
#include "skgtestmacro.h"

/**
 * The main function of the unit test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true) {
        // Test load old version of files
        {
            SKGDocumentBank document1;
            SKGTESTERROR(QStringLiteral("DOC:load"), document1.load(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestmigration/version_0.1.skg"), true)
        }
        {
            SKGDocumentBank document1;
            SKGTESTERROR(QStringLiteral("DOC:load"), document1.load(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestmigration/version_0.3.skg"), true)
        }
        {
            SKGDocumentBank document1;
            SKGTESTERROR(QStringLiteral("DOC:load"), document1.load(SKGTest::getTestPath(QStringLiteral("IN")) % "/skgtestmigration/version_1.12.skg"), true)
        }
    }

    // End test
    SKGENDTEST()
}
