/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */

#include <qdesktopservices.h>

#include "skgbankincludes.h"
#include "skgreportbank.h"
#include "skgtestmacro.h"

/**
 * The main function of the bank report test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true)

    {
        // Test unit et unitvalue
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.load()"), document1.load(SKGTest::getTestPath(QStringLiteral("IN")) % QStringLiteral("/advice.skg")), true)
        auto* rep = qobject_cast<SKGReportBank*>(document1.getReport());
        rep->setPeriod(QStringLiteral("2013"));
        rep->get5MainCategoriesVariation();
        rep->getAlarms();
        rep->getInterests();
        rep->getAccountTable();
        rep->getBankTable();
        rep->getBudgetTable();
        rep->getIncomeVsExpenditure();
        rep->getIncomeVsExpenditure();
        rep->getMainCategoriesForPeriod();
        rep->getMainCategoriesForPreviousPeriod();
        rep->get5MainCategoriesVariationIssue();
        rep->getPortfolio();
        rep->getScheduledOperations();
        rep->getUnitTable();
        rep->getNetWorth();
        rep->getAnnualSpending();
        rep->getPersonalFinanceScore();

        rep->getPrevious();

        rep->getTipOfDay();
        rep->getTipsOfDay();

        rep->setTipsOfDay(QStringList() << QStringLiteral("Hello") << QStringLiteral("world"));

        rep->getTipOfDay();
        rep->getTipsOfDay();

        rep->setPointSize(10);
        SKGTEST(QStringLiteral("REP:getPointSize"), rep->getPointSize(), 10)

        QString html;
        SKGTESTERROR(QStringLiteral("SKGReportBank::getReportFromTemplate"), SKGReportBank::getReportFromTemplate(rep, SKGTest::getTestPath(QStringLiteral("IN")) % QStringLiteral("/template.txt"), html), true)
        delete rep;
    }
    // End test
    SKGENDTEST()
}
