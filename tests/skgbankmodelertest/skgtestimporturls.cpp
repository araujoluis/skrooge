/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestmacro.h"
#include "skgbankincludes.h"
#include "skgimportexportmanager.h"

/**
 * The main function of the unit test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true)

    QStringList extensions;
    extensions << QStringLiteral("csv") << QStringLiteral("cfo") << QStringLiteral("gnucash") << QStringLiteral("gsb") << QStringLiteral("kmy") << QStringLiteral("mmb") << QStringLiteral("mt940") << QStringLiteral("ofx") << QStringLiteral("qif") << QStringLiteral("skg") << QStringLiteral("xhb");

    int nb = extensions.count();
    for (int i = 0; i < nb; ++i) {
        const QString& ext = extensions.at(i);
        QString filename = "https://skrooge.org/files/skgtestimporturl/test." % ext;
        SKGTRACE << i + 1 << "/" << nb << ": Import " << filename << endl;

        // Test import
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true)
        SKGError err;
        {
            // Scope of the transaction
            SKGBEGINTRANSACTION(document1, QStringLiteral("IMPORT"), err)

            SKGImportExportManager imp1(&document1, QUrl::fromUserInput(filename));
            SKGTESTERROR(ext % ".importFile", imp1.importFile(), true)
        }

        if (ext == QStringLiteral("skg") && SKGServices::getEnvVariable(QStringLiteral("USER")) == QStringLiteral("s")) {
            QStringList extensionsExport;
            extensionsExport.push_back(QStringLiteral("csv"));
            extensionsExport.push_back(QStringLiteral("kmy"));
            extensionsExport.push_back(QStringLiteral("qif"));
            extensionsExport.push_back(QStringLiteral("skg"));
            extensionsExport.push_back(QStringLiteral("xml"));
            extensionsExport.push_back(QStringLiteral("sqlite"));

            int nb2 = extensionsExport.count();
            for (int j = 0; j < nb2; ++j) {
                const QString& ext2 = extensionsExport.at(j);
                QString filename2 = "ftp://skrooge.org/files/skgtestimporturl/output/test." % ext2;
                SKGTRACE << "    " << j + 1 << "/" << nb2 << ": Export " << filename2 << endl;

                // Scope of the transaction
                SKGBEGINTRANSACTION(document1, QStringLiteral("EXPORT"), err)

                SKGImportExportManager imp1(&document1,  QUrl::fromUserInput(filename2));
                SKGTESTERROR(ext2 % ".exportFile", imp1.exportFile(), true)
            }
        }
    }

    {
        SKGDocumentBank document1;
        SKGTESTERROR(QStringLiteral("document1.initialize()"), document1.initialize(), true)
        SKGTESTERROR(QStringLiteral("document1.load()"), document1.load(QStringLiteral("https://skrooge.org/files/skgtestimporturl/test.skg")), true)
    }

    // End test
    SKGENDTEST()
}
