/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test for SKGDateEdit component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestdateedit.h"

#include <qtestevent.h>
#include <qtestkeyboard.h>

#include "skgdateedit.h"

void SKGTESTDateEdit::Test_data()
{
    QTest::addColumn<QTestEventList>("events");
    QTest::addColumn<QDate>("expected");

    // Day
    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_Up);
        list.addKeyClick(Qt::Key_Up);
        QTest::newRow("++") << list << QDate(1970, 7, 18);
    }

    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_Up);
        list.addKeyClick(Qt::Key_Down);
        QTest::newRow("+-") << list << QDate(1970, 7, 16);
    }

    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_Down);
        list.addKeyClick(Qt::Key_Down);
        QTest::newRow("--") << list << QDate(1970, 7, 14);
    }

    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_Down);
        list.addKeyClick(Qt::Key_Up);
        QTest::newRow("-+") << list << QDate(1970, 7, 16);
    }

    // Month
    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_PageUp);
        list.addKeyClick(Qt::Key_PageUp);
        QTest::newRow("++ctrl") << list << QDate(1970, 9, 16);
    }

    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_PageUp);
        list.addKeyClick(Qt::Key_PageDown);
        QTest::newRow("+-ctrl") << list << QDate(1970, 7, 16);
    }

    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_PageDown);
        list.addKeyClick(Qt::Key_PageDown);
        QTest::newRow("--ctrl") << list << QDate(1970, 5, 16);
    }

    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_PageDown);
        list.addKeyClick(Qt::Key_PageUp);
        QTest::newRow("-+ctrl") << list << QDate(1970, 7, 16);
    }

    // Today
    {
        QTestEventList list;
        list.addKeyClick(Qt::Key_Equal);
        QTest::newRow("=") << list << QDate::currentDate();
    }
}

void SKGTESTDateEdit::Test()
{
    QFETCH(QTestEventList, events);
    QFETCH(QDate, expected);

    SKGDateEdit dateEditor(nullptr);
    dateEditor.setDate(QDate(1970, 7, 16));
    events.simulate(&dateEditor);
    dateEditor.mode();

    QCOMPARE(dateEditor.date(), expected);
}

QTEST_MAIN(SKGTESTDateEdit)

