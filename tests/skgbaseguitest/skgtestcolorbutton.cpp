/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test for SKGDateEdit component.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestcolorbutton.h"
#include "skgcolorbutton.h"

void SKGTESTColorButton::Test()
{
    SKGColorButton color(nullptr);
    color.setText(QStringLiteral("Hello"));
    QCOMPARE(color.text(), QStringLiteral("Hello"));
    color.setColor(Qt::white);
    QCOMPARE(color.color(), QColor(Qt::white));
    color.setDefaultColor(Qt::black);
    QCOMPARE(color.defaultColor(), QColor(Qt::black));
}

QTEST_MAIN(SKGTESTColorButton)

