/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a test script.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtestmacro.h"

/**
 * The main function of the unit test
 * @param argc the number of arguments
 * @param argv the list of arguments
 */
int main(int argc, char** argv)
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    // Init test
    SKGINITTEST(true)
    QString filenameInput1 = SKGTest::getTestPath(QStringLiteral("IN")) % "/all_plugins.skg";
    QString filenameOutput1 = SKGTest::getTestPath(QStringLiteral("OUT")) % "/all_plugins_encrypted.skg";
    QString filenameOutput2 = SKGTest::getTestPath(QStringLiteral("OUT")) % "/all_plugins_decrypted.skg";

    bool mode;
    SKGTESTERROR(QStringLiteral("DOC:cryptFile"), SKGServices::cryptFile(QStringLiteral("notfound"), filenameOutput1, QStringLiteral("password"), true, QStringLiteral("SKROOGE"), mode), false)
    SKGTESTERROR(QStringLiteral("DOC:cryptFile"), SKGServices::cryptFile(filenameInput1, filenameOutput1, QStringLiteral("password"), true, QStringLiteral("SKROOGE"), mode), true)
    SKGTESTERROR(QStringLiteral("DOC:cryptFile"), SKGServices::cryptFile(filenameOutput1, filenameOutput2, QStringLiteral("password"), false, QStringLiteral("SKROOGE"), mode), true)
    SKGTESTERROR(QStringLiteral("DOC:cryptFile"), SKGServices::cryptFile(QStringLiteral("notfound"), filenameOutput1, QStringLiteral("password"), false, QStringLiteral("SKROOGE"), mode), false)

    // End test
    SKGENDTEST()
}
