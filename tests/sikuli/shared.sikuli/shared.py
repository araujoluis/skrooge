#***************************************************************************
#*   Copyright (C) 2008 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program.  If not, see <http://www.gnu.org/licenses/>  *
#***************************************************************************
import os
import shutil
from sikuli.Sikuli import *

def openFile(fileName):
	print("Opening "+fileName)
	type("o", KEY_CTRL)
	if exists("1446152114435.png"):
		click("1446152575740.png")
	paste(Pattern("1446149387754.png").similar(0.59), fileName)
	click("1446152645440.png")
	
def openAllPages():	
	type("1", KEY_CTRL)
	type("2", KEY_CTRL)
	type("3", KEY_CTRL)
	type("4", KEY_CTRL)
	type("5", KEY_CTRL)
	type("6", KEY_CTRL)
	type("7", KEY_CTRL)
	type("8", KEY_CTRL)
	type("9", KEY_CTRL)
	type("0", KEY_CTRL+KEY_ALT)	
	type("1", KEY_CTRL+KEY_ALT)	
	type("2", KEY_CTRL+KEY_ALT)	
	type("3", KEY_CTRL+KEY_ALT)	
	
def closeCurrentPage():
	type("w", KEY_CTRL)

def initSimple():
	print("Initialisation with "+Env.getSikuliVersion())
	App.open("skrooge  > /dev/null 2>&1")
	sleep(5)
	#click("1446152344094.png")	

def initAllPlugins():
	initSimple()
	openFile(os.getenv('IN')+"all_plugins.skg")
	sleep(5)
	click("1446152986402.png")

def close():
	App.close("skrooge")

def undo():
    type("z", KEY_CTRL)

def redo():
    type("z", KEY_CTRL+KEY_SHIFT)

def openSettings():
	click("Setting.png")
	click("gunfigureSrg.png")
	click("Vox.png")
	
def createAccount(bankName, accountName):
	type("2", KEY_CTRL)
	paste("1383133694348.png", bankName)
	paste("1383133566859.png", accountName)
	sleep(1)
	type(Key.ENTER, KEY_CTRL)

	closeCurrentPage()

def openReport():
	type("r", KEY_META)

def generateErrorCapture(name):
    img=capture(SCREEN)
    errorCapture=os.path.join(os.getenv('OUT'), name+"/error.png")
    shutil.move(img, errorCapture)
    print "Capture at failure:"+errorCapture
