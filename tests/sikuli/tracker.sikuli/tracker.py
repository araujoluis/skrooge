#***************************************************************************
#*   Copyright (C) 2008 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program.  If not, see <http://www.gnu.org/licenses/>  *
#***************************************************************************

# works on all platforms
import os

# get the directory containing your running .sikuli
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)
import shared
try:
    setAutoWaitTimeout(10)
    
    shared.initSimple()
    
    click("1305985754504.png")
    paste(Pattern("1305985800311.png").similar(0.87), "tracker1")
    sleep(1)
    type(Key.ENTER, KEY_CTRL)
    paste("Nanatradar.png", "_b")
    sleep(1)
    type(Key.ENTER, KEY_SHIFT)
    doubleClick("CtracIer1b00.png")
    r=find("Operations-1.png").right()
    c=r.find("1305987216474.png")
    click(c)
    click("1305986344895.png")
    click("ShowOpened.png")
    click("Closed.png")
    
    
    shared.close()
    pass
except FindFailed:
    shared.generateErrorCapture("tracker")
    raise
