#***************************************************************************
#*   Copyright (C) 2008 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program.  If not, see <http://www.gnu.org/licenses/>  *
#***************************************************************************
# works on all platforms
import os

# get the directory containing your running .sikuli
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)
import shared
try:
    setAutoWaitTimeout(10)
    
    shared.initAllPlugins()
    sleep(5)
    click("1306052486290.png")
    status=find("YStatus.png")
    doubleClick(status)
    wait(1)
    doubleClick(status.nearby(400).find(Pattern("Status-1.png").similar(0.81)))
    click("1306052650498.png")
    click("1306052703527.png")
    sleep(1)
    type(Key.ENTER, KEY_CTRL)
    sleep(1)
    type(Key.ENTER, KEY_SHIFT)
    click("1306053834224.png")
    click("1306053853927.png")
    click("1306053869838.png")
    click("1306053885688.png")
    
    click("1306055505239.png")
    r=find("Operations-1.png").right()
    click(r.find("1305987216474.png"))
    
    click("Update.png")
    status2=status.below().find("YStatus.png")
    doubleClick(status2)
    wait(1)
    doubleClick(status2.nearby(400).find(Pattern("Status-1.png").similar(0.81)))
    click("1306052650498.png")
    click("1306052703527.png")
    click("1305986344895.png")
    sleep(1)
    type(Key.ENTER, KEY_SHIFT)
    
    shared.close()
    pass
except FindFailed:
    shared.generateErrorCapture("search")
    raise
