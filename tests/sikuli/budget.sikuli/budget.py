#***************************************************************************
#*   Copyright (C) 2008 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program.  If not, see <http://www.gnu.org/licenses/>  *
#***************************************************************************
# works on all platforms
import os

# get the directory containing your running .sikuli
myPath = os.path.dirname(getBundlePath())
if not myPath in sys.path: sys.path.append(myPath)
import shared
try:
    setAutoWaitTimeout(10)
    
    shared.initAllPlugins()
    
    click("1446153094651.png")
    click("1446153115180.png")
    click("1446153142941.png")
    sleep(1)
    type(Key.ENTER, KEY_CTRL)
    click("1446155010281.png")
    sleep(2)
    type(Key.ENTER, KEY_CTRL)
    click("1446654721017.png")
    
    wait(10)
    
    shared.undo()
    shared.redo()
    
    shared.close()
    pass
except FindFailed:
    shared.generateErrorCapture("budget")
    raise
