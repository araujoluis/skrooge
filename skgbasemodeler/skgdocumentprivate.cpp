/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file implements classes SKGDocumentPrivate.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgdocumentprivate.h"

/**
 * Last error.
 */
SKGError SKGDocumentPrivate::m_lastCallbackError;

/**
 * Unique identifier.
 */
int SKGDocumentPrivate::m_databaseUniqueIdentifier = 0;

SKGDocumentPrivate::SKGDocumentPrivate()
    : m_currentFileName(QLatin1String(""))
{
    m_cacheSql = new QHash<QString, SKGStringListList>();
}

SKGDocumentPrivate::~SKGDocumentPrivate()
{
    delete m_cacheSql;
    m_cacheSql = nullptr;
}
