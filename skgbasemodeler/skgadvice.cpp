/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
* This file implements classes SKGAdvice.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include "skgadvice.h"
#include "skgdefine.h"

SKGAdvice::SKGAdvice() :  QObject()
{}

SKGAdvice::SKGAdvice(const SKGAdvice& iAdvice)
    :  QObject(), m_uuid(iAdvice.m_uuid), m_priority(iAdvice.m_priority),
       m_shortMessage(iAdvice.m_shortMessage), m_longMessage(iAdvice.m_longMessage),
       m_autoCorrections(iAdvice.m_autoCorrections)

{}

SKGAdvice::~SKGAdvice()
    = default;

SKGAdvice& SKGAdvice::operator= (const SKGAdvice& iAdvice)
{
    if (&iAdvice != this) {
        m_priority = iAdvice.m_priority;
        m_shortMessage = iAdvice.m_shortMessage;
        m_longMessage = iAdvice.m_longMessage;
        m_autoCorrections = iAdvice.m_autoCorrections;
        m_uuid = iAdvice.m_uuid;
        Q_EMIT modified();
    }
    return *this;
}

void SKGAdvice::setUUID(const QString& iUUID)
{
    if (m_uuid != iUUID) {
        m_uuid = iUUID;
        Q_EMIT modified();
    }
}

QString SKGAdvice::getUUID() const
{
    return m_uuid;
}

void SKGAdvice::setPriority(int iPriority)
{
    if (m_priority != iPriority) {
        m_priority = iPriority;
        Q_EMIT modified();
    }
}

int SKGAdvice::getPriority() const
{
    return m_priority;
}

void SKGAdvice::setShortMessage(const QString& iMessage)
{
    if (m_shortMessage != iMessage) {
        m_shortMessage = iMessage;
        Q_EMIT modified();
    }
}

QString SKGAdvice::getShortMessage() const
{
    return m_shortMessage;
}

void SKGAdvice::setLongMessage(const QString& iMessage)
{
    if (m_longMessage != iMessage) {
        m_longMessage = iMessage;
        Q_EMIT modified();
    }
}

QString SKGAdvice::getLongMessage() const
{
    return m_longMessage;
}

void SKGAdvice::setAutoCorrections(const QStringList& iCorrections)
{
    SKGAdvice::SKGAdviceActionList tmp;
    tmp.reserve(iCorrections.count());
    for (const auto& c : qAsConst(iCorrections)) {
        SKGAdviceAction a;
        a.Title = c;
        a.IsRecommended = false;
        tmp.push_back(a);
    }

    setAutoCorrections(tmp);
}

void SKGAdvice::setAutoCorrections(const SKGAdvice::SKGAdviceActionList& iCorrections)
{
    m_autoCorrections = iCorrections;
    Q_EMIT modified();
}

SKGAdvice::SKGAdviceActionList SKGAdvice::getAutoCorrections() const
{
    return m_autoCorrections;
}
