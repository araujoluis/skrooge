/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a predicat creator for skrooge (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgunitcomboboxdesignerplugin.h"

#include <qicon.h>



#include "skgunitcombobox.h"

SKGUnitComboBoxDesignerPlugin::SKGUnitComboBoxDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGUnitComboBoxDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGUnitComboBoxDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGUnitComboBoxDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGUnitComboBox(iParent);
}

QString SKGUnitComboBoxDesignerPlugin::name() const
{
    return QStringLiteral("SKGUnitComboBox");
}

QString SKGUnitComboBoxDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGUnitComboBoxDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("skrooge.png"));
}

QString SKGUnitComboBoxDesignerPlugin::toolTip() const
{
    return QStringLiteral("A unit combo box");
}

QString SKGUnitComboBoxDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A unit combo box");
}

bool SKGUnitComboBoxDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGUnitComboBoxDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGUnitComboBox\" name=\"SKGUnitComboBox\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>100</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGUnitComboBoxDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgunitcombobox.h");
}

