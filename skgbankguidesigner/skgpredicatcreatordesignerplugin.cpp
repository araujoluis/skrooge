/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a predicat creator for skrooge (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgpredicatcreatordesignerplugin.h"



#include "skgpredicatcreator.h"
#include "skgservices.h"

SKGPredicatCreatorDesignerPlugin::SKGPredicatCreatorDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGPredicatCreatorDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGPredicatCreatorDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGPredicatCreatorDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGPredicatCreator(iParent, nullptr);
}

QString SKGPredicatCreatorDesignerPlugin::name() const
{
    return QStringLiteral("SKGPredicatCreator");
}

QString SKGPredicatCreatorDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGPredicatCreatorDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("skrooge.png"));
}

QString SKGPredicatCreatorDesignerPlugin::toolTip() const
{
    return QStringLiteral("A predicat creator");
}

QString SKGPredicatCreatorDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A predicat creator");
}

bool SKGPredicatCreatorDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGPredicatCreatorDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGPredicatCreator\" name=\"SKGPredicatCreator\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>100</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGPredicatCreatorDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgpredicatcreator.h");
}

