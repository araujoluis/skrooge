/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a query creator for skrooge (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgquerycreatordesignerplugin.h"



#include "skgquerycreator.h"
#include "skgservices.h"

SKGQueryCreatorDesignerPlugin::SKGQueryCreatorDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGQueryCreatorDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGQueryCreatorDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGQueryCreatorDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGQueryCreator(iParent);
}

QString SKGQueryCreatorDesignerPlugin::name() const
{
    return QStringLiteral("SKGQueryCreator");
}

QString SKGQueryCreatorDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGQueryCreatorDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("skrooge.png"));
}

QString SKGQueryCreatorDesignerPlugin::toolTip() const
{
    return QStringLiteral("A query creator");
}

QString SKGQueryCreatorDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A query creator");
}

bool SKGQueryCreatorDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGQueryCreatorDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGQueryCreator\" name=\"SKGQueryCreator\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>100</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGQueryCreatorDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgquerycreator.h");
}

