/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A color button with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgcolorbutton.h"

SKGColorButton::SKGColorButton(QWidget* iParent)
    : QWidget(iParent)
{
    ui.setupUi(this);
    connect(ui.button, &KColorButton::changed, this, &SKGColorButton::changed);
}

SKGColorButton::~SKGColorButton()
    = default;

QString SKGColorButton::text() const
{
    return m_text;
}

void SKGColorButton::setText(const QString& iText)
{
    m_text = iText;
    ui.label->setText(iText);
}

QColor SKGColorButton::color() const
{
    return ui.button->color();
}

void SKGColorButton::setColor(const QColor& iColor)
{
    ui.button->setColor(iColor);
}

QColor SKGColorButton::defaultColor() const
{
    return ui.button->defaultColor();
}

void SKGColorButton::setDefaultColor(const QColor& iColor)
{
    ui.button->setDefaultColor(iColor);
}



