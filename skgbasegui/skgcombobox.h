/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGCOMBOBOX_H
#define SKGCOMBOBOX_H
/** @file
 * A combo box with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */

#include "skgbasegui_export.h"
#include <kcombobox.h>
/**
 * This file is a combo box with more features.
 */
class SKGBASEGUI_EXPORT SKGComboBox : public KComboBox
{
    Q_OBJECT
    /**
     * Text of the combobox
     */
    Q_PROPERTY(QString text READ text WRITE setText USER true)   // clazy:exclude=qproperty-without-notify

public:
    /**
     * Default Constructor
     * @param iParent the parent
     */
    explicit SKGComboBox(QWidget* iParent = nullptr);

    /**
     * Default Destructor
     */
    ~SKGComboBox() override;

    /**
     * Get the text for the combo
     * @return the text
     */
    virtual QString text() const;

    /**
     * Set the text for the combo
     * @param iText the text
     */
    virtual void setText(const QString& iText);

    /**
     * Set the Palette for the combobox.
     * Reimplemented since the base method does
     * not apply the Palette to the underlying
     * QlineEdit
     * @param iPalette the new palette
     */
    virtual void setPalette(const QPalette& iPalette);
};

#endif  // SKGCOMBOBOX_H
