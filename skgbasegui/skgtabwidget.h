/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGTABWIDGET_H
#define SKGTABWIDGET_H
/** @file
 * A QTabWidget with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <qhash.h>
#include <qtabwidget.h>
#include <qtimer.h>

#include "skgbasegui_export.h"

class QPushButton;

/**
 * A QTabWidget with more features.
 */
class SKGBASEGUI_EXPORT SKGTabWidget : public QTabWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent
     */
    explicit SKGTabWidget(QWidget* iParent);

    /**
     * Default Destructor
     */
    ~SKGTabWidget() override;

public Q_SLOTS:
    /**
     * Remove a tab
     * @param index the tab index
     */
    virtual void removeTab(int index);

private Q_SLOTS:
    void onCurrentChanged();
    void onRefreshSaveIcon();
    void onSaveRequested();
    void onMoveTab(int oldPos, int newPos);

private:
    QTimer m_timerSave;

    QHash<QWidget*, QPushButton*> m_tabIndexSaveButton;
};

#endif  // SKGTABWIDGET_H
