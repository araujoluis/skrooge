/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is a class managing widget.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgwidget.h"

#include <qwidget.h>

#include "skgtraces.h"
#include "skgtreeview.h"

SKGWidget::SKGWidget(QWidget* iParent, SKGDocument* iDocument)
    : QWidget(iParent), m_document(iDocument)
{
    SKGTRACEINFUNC(5)
}

SKGWidget::~SKGWidget()
{
    SKGTRACEINFUNC(5)
    m_document = nullptr;
}

SKGDocument* SKGWidget::getDocument() const
{
    return m_document;
}

QString SKGWidget::getState()
{
    return QLatin1String("");
}

QString SKGWidget::getDefaultStateAttribute()
{
    return QLatin1String("");
}

void SKGWidget::setState(const QString& /*iState*/)
{
}

SKGObjectBase::SKGListSKGObjectBase SKGWidget::getSelectedObjects()
{
    SKGObjectBase::SKGListSKGObjectBase selection;
    auto* treeView = qobject_cast<SKGTreeView*>(mainWidget());
    if (treeView != nullptr) {
        selection = treeView->getSelectedObjects();
    }

    return selection;
}

SKGObjectBase SKGWidget::getFirstSelectedObject()
{
    SKGObjectBase first;
    auto* treeView = qobject_cast<SKGTreeView*>(mainWidget());
    if (treeView != nullptr) {
        first = treeView->getFirstSelectedObject();
    }

    return first;
}

int SKGWidget::getNbSelectedObjects()
{
    int output = 0;
    auto* treeView = qobject_cast<SKGTreeView*>(mainWidget());
    if (treeView != nullptr) {
        output = treeView->getNbSelectedObjects();
    } else {
        output = getSelectedObjects().count();
    }

    return output;
}

bool SKGWidget::hasSelectionWithFocus()
{
    return (mainWidget()->hasFocus());
}

bool SKGWidget::eventFilter(QObject* iObject, QEvent* iEvent)
{
    if (iObject == mainWidget() && (iEvent != nullptr) && (iEvent->type() == QEvent::FocusIn || iEvent->type() == QEvent::FocusOut)) {
        emit selectionFocusChanged();
    }
    return QObject::eventFilter(iObject, iEvent);
}

QWidget* SKGWidget::mainWidget()
{
    return this;
}


