/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGWEBVIEW_H
#define SKGWEBVIEW_H
/** @file
 * A web viewer with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <qprinter.h>
#ifdef SKG_WEBENGINE
#include <qwebengineview.h>
#else
#include <qwebview.h>
#endif
#include "skgbasegui_export.h"


/**
 * This file is a web viewer
 */
#ifdef SKG_WEBENGINE
class SKGBASEGUI_EXPORT SKGWebView : public QWebEngineView
#else
class SKGBASEGUI_EXPORT SKGWebView : public QWebView
#endif
{
    Q_OBJECT

public:
    /**
     * Constructor
     * @param iParent the parent
     * @param name name
     */
#ifdef SKG_WEBENGINE
    explicit SKGWebView(QWidget* iParent, const char* name = nullptr, bool iWithContextualMenu = true);

    /**
     * To emit that a link is clicked
     * @param iURL the url of the link
     */
    void emitLinkClicked(const QUrl& iURL);

#else
    explicit SKGWebView(QWidget* iParent, const char* name = nullptr);
#endif

    /**
     * Destructor
     */
    ~SKGWebView() override;

    /**
     * Get the current state
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    virtual QString getState();

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    virtual void setState(const QString& iState);

    /**
     * Export in the file (PDF, ODT, png, jpeg, ...)
     * @param iFileName the file name
     */
    virtual void exportInFile(const QString& iFileName);

Q_SIGNALS:
    /**
     * Emitted when zoom changed
     * @param iZoomPosition zoom position (-10<=zoom position<=10)
     */
    void zoomChanged(int iZoomPosition);

    /**
     * Emitted when a file is exported
     * @param iFileName the exported file name
     */
    void fileExporter(const QString& iFileName);

#ifdef SKG_WEBENGINE
    /**
     * Emitted when a link is clicked
     * @param iURL the url of the link
     */
    void linkClicked(const QUrl& iURL);
#endif

public Q_SLOTS:
    /**
     * Zoom in
     */
    virtual void onZoomIn();

    /**
     * Zoom out
     */
    virtual void onZoomOut();

    /**
     * Fit on scene
     */
    virtual void onZoomOriginal();

    /**
     * Print
     */
    virtual void onPrint();

    /**
     * Export
     */
    virtual void onExport();

    /**
     * Print preview
     */
    virtual void onPrintPreview();

protected:
    /**
     * Contextual event
     * @param iEvent the event
     */
    void contextMenuEvent(QContextMenuEvent* iEvent) override;

    /**
     * Event filtering
     * @param iObject object
     * @param iEvent event
     * @return In your reimplementation of this function, if you want to filter the event out, i.e. stop it being handled further, return true; otherwise return false.
     */
    bool eventFilter(QObject* iObject, QEvent* iEvent) override;

private:
    Q_DISABLE_COPY(SKGWebView)
    QPrinter m_printer;

#ifdef SKG_WEBENGINE
    bool m_ContextualMenu;
#else
    void openReply(QNetworkReply* reply);
#endif
};

#endif
