/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A date editor with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgdateedit.h"

#include <kdatevalidator.h>
#include <qlineedit.h>

#include <klocalizedstring.h>

SKGDateEdit::SKGDateEdit(QWidget* iParent, const char* name)
    : KPIM::KDateEdit(iParent), m_mode(CURRENT)
{
    setObjectName(name);
    setMode(CURRENT);
    setToolTip(i18n("Date of the operation\nup or down to add or remove one day\nCTRL + up or CTRL + down to add or remove one month"));
}

SKGDateEdit::~SKGDateEdit()
    = default;

SKGDateEdit::Mode SKGDateEdit::mode() const
{
    return m_mode;
}

void SKGDateEdit::setMode(Mode iMode)
{
    if (iMode != m_mode) {
        m_mode = iMode;

        auto* val = qobject_cast<KPIM::KDateValidator*>(const_cast<QValidator*>(validator()));
        val->setFixupBehavior(m_mode == CURRENT ? KPIM::KDateValidator::FixupCurrent : m_mode == NEXT ? KPIM::KDateValidator::FixupForward : KPIM::KDateValidator::FixupBackward);

        emit modeChanged();
    }
}


