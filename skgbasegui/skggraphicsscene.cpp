/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
* This file defines classes SKGGraphicsScene.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skggraphicsscene.h"

SKGGraphicsScene::SKGGraphicsScene(QObject* iParent)
    : QGraphicsScene(iParent)
{
}

SKGGraphicsScene::~SKGGraphicsScene()
    = default;

void SKGGraphicsScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
    Q_EMIT doubleClicked();

    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
}


