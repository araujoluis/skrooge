/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGTABLEVIEW_H
#define SKGTABLEVIEW_H
/** @file
 * A table view with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgbasegui_export.h"
#include "skgtreeview.h"

/**
 * This file is a tab widget used by plugins
 */
class SKGBASEGUI_EXPORT SKGTableView : public SKGTreeView
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     * @param iParent the parent
     */
    explicit SKGTableView(QWidget* iParent);

    /**
     * Default Destructor
     */
    ~SKGTableView() override;
};

#endif  // SKGTABLEVIEW_H
