/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGGRAPHICSSCENE_H
#define SKGGRAPHICSSCENE_H
/** @file
 * This file defines a graphic scene with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */

#include "skgbasegui_export.h"
#include <qgraphicsscene.h>
/**
 * A graphic scene with more features
 */
class SKGBASEGUI_EXPORT SKGGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    /**
     * Default constructor
     * @param iParent the parent
     */
    explicit SKGGraphicsScene(QObject* iParent = nullptr);

    /**
     * Destructor
     */
    ~SKGGraphicsScene() override;

Q_SIGNALS:
    /**
     * This signal is launched when a double click is done
     */
    void doubleClicked();

protected:
    /**
     * This event handler, for event mouseEvent, can be reimplemented in a subclass to receive mouse doubleclick events for the scene.
     * @param mouseEvent the event
     */
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* mouseEvent) override;

private:
    Q_DISABLE_COPY(SKGGraphicsScene)
};

#endif
