/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A combo box with more features.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgcombobox.h"

#include <qlineedit.h>

SKGComboBox::SKGComboBox(QWidget* iParent)
    : KComboBox(iParent)
{
}

SKGComboBox::~SKGComboBox()
    = default;

QString SKGComboBox::text() const
{
    return currentText();
}

void SKGComboBox::setText(const QString& iText)
{
    int pos2 = findText(iText);
    if (pos2 == -1) {
        pos2 = 0;
        insertItem(pos2, iText);
    }
    setCurrentIndex(pos2);
}

void SKGComboBox::setPalette(const QPalette& iPalette)
{
    KComboBox::setPalette(iPalette);
    lineEdit()->setPalette(iPalette);
}

