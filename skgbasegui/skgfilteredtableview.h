/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGFILTEREDTABLEVIEW_H
#define SKGFILTEREDTABLEVIEW_H
/** @file
 * A filetered SKGTableView.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <qtimer.h>
#include <qwidget.h>

#include "skgbasegui_export.h"
#include "ui_skgfilteredtableview.h"

class SKGShow;
class SKGObjectModelBase;

/**
 * This file is a filetered SKGTableView
 */
class SKGBASEGUI_EXPORT SKGFilteredTableView : public QWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent
     */
    explicit SKGFilteredTableView(QWidget* iParent);

    /**
     * Default Destructor
     */
    ~SKGFilteredTableView() override;

    /**
     * Get the current state
     * @return a string containing all activated item identifiers (separated by ;)
     */
    virtual QString getState();

    /**
     * Set the current state
     * @param iState a string containing all activated item identifiers (separated by ;)
     */
    virtual void setState(const QString& iState);

    /**
     * @brief Get show widget
     *
     * @return SKGShow
     **/
    virtual SKGShow* getShowWidget() const;

    /**
     * @brief Get table or tree view
     *
     * @return SKGTreeView
     **/
    virtual SKGTreeView* getView() const;

    /**
     * @brief Get the search field
     *
     * @return QLineEdit
     **/
    virtual QLineEdit* getSearchField() const;

    /**
     * @brief Set model
     *
     * @param iModel the model
     * @return void
     **/
    virtual void setModel(SKGObjectModelBase* iModel);

public Q_SLOTS:
    /**
     * @brief Set the filter. This filter disable the "show" menu
     *
     * @param iIcon the icon
     * @param iText the text to display
     * @param iWhereClause the where clause
     * @return void
     **/
    virtual void setFilter(const QIcon& iIcon, const QString& iText, const QString& iWhereClause);

    /**
     * @brief Reset the filter
     * @return void
     **/
    virtual void resetFilter();

private Q_SLOTS:
    void pageChanged();
    void onFilterChanged();
    void onTextFilterChanged();
    void dataModified(const QString& iTableName, int iIdTransaction);

private:
    Q_DISABLE_COPY(SKGFilteredTableView)

    Ui::skgfilteredtableview_base ui{};
    SKGObjectModelBase* m_objectModel;
    bool m_refreshNeeded;
    QTimer m_timer;
};

#endif  // SKGFILTEREDTABLEVIEW_H
