/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A collection of widgets (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgwidgetcollectiondesignerplugin.h"

#include "skgcalculatoreditdesignerplugin.h"
#include "skgcolorbuttondesignerplugin.h"
#include "skgcomboboxdesignerplugin.h"
#include "skgdateeditdesignerplugin.h"
#include "skgfilteredtableviewdesignerplugin.h"
#include "skggraphicsviewdesignerplugin.h"
#include "skgperiodeditdesignerplugin.h"
#include "skgprogressbardesignerplugin.h"
#include "skgshowdesignerplugin.h"
#include "skgsimpleperiodeditdesignerplugin.h"
#include "skgtableviewdesignerplugin.h"
#include "skgtablewidgetdesignerplugin.h"
#include "skgtablewithgraphdesignerplugin.h"
#include "skgtabwidgetdesignerplugin.h"
#include "skgtreeviewdesignerplugin.h"
#include "skgwebviewdesignerplugin.h"
#include "skgwidgetselectordesignerplugin.h"
#include "skgzoomselectordesignerplugin.h"

SKGWidgetCollectionDesignerPlugin::SKGWidgetCollectionDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_widgets.append(new SKGCalculatorEditDesignerPlugin(this));
    m_widgets.append(new SKGComboBoxDesignerPlugin(this));
    m_widgets.append(new SKGColorButtonDesignerPlugin(this));
    m_widgets.append(new SKGDateEditDesignerPlugin(this));
    m_widgets.append(new SKGFilteredTableViewDesignerPlugin(this));
    m_widgets.append(new SKGGraphicsViewDesignerPlugin(this));
    m_widgets.append(new SKGShowDesignerPlugin(this));
    m_widgets.append(new SKGTableViewDesignerPlugin(this));
    m_widgets.append(new SKGTreeViewDesignerPlugin(this));
    m_widgets.append(new SKGTableWithGraphDesignerPlugin(this));
    m_widgets.append(new SKGTabWidgetDesignerPlugin(this));
    m_widgets.append(new SKGTableWidgetDesignerPlugin(this));
    m_widgets.append(new SKGWebViewDesignerPlugin(this));
    m_widgets.append(new SKGWidgetSelectorDesignerPlugin(this));
    m_widgets.append(new SKGZoomSelectorDesignerPlugin(this));
    m_widgets.append(new SKGProgressBarDesignerPlugin(this));
    m_widgets.append(new SKGPeriodEditDesignerPlugin(this));
    m_widgets.append(new SKGSimplePeriodEditDesignerPlugin(this));
}

QList<QDesignerCustomWidgetInterface*> SKGWidgetCollectionDesignerPlugin::customWidgets() const
{
    return m_widgets;
}
