/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A widget selector (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgwidgetselectordesignerplugin.h"

#include "skgservices.h"
#include "skgwidgetselector.h"

SKGWidgetSelectorDesignerPlugin::SKGWidgetSelectorDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGWidgetSelectorDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGWidgetSelectorDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGWidgetSelectorDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGWidgetSelector(iParent);
}

QString SKGWidgetSelectorDesignerPlugin::name() const
{
    return QStringLiteral("SKGWidgetSelector");
}

QString SKGWidgetSelectorDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGWidgetSelectorDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("quickopen"));
}

QString SKGWidgetSelectorDesignerPlugin::toolTip() const
{
    return QStringLiteral("A widget selector");
}

QString SKGWidgetSelectorDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A widget selector");
}

bool SKGWidgetSelectorDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGWidgetSelectorDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGWidgetSelector\" name=\"SKGWidgetSelector\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>10</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGWidgetSelectorDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgwidgetselector.h");
}

