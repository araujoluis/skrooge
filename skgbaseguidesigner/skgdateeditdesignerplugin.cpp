/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A date edit with more features (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgdateeditdesignerplugin.h"



#include "skgdateedit.h"
#include "skgservices.h"

SKGDateEditDesignerPlugin::SKGDateEditDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGDateEditDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGDateEditDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGDateEditDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGDateEdit(iParent);
}

QString SKGDateEditDesignerPlugin::name() const
{
    return QStringLiteral("SKGDateEdit");
}

QString SKGDateEditDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGDateEditDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("quickopen"));
}

QString SKGDateEditDesignerPlugin::toolTip() const
{
    return QStringLiteral("A date editor with more features");
}

QString SKGDateEditDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A date editor with more features");
}

bool SKGDateEditDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGDateEditDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGDateEdit\" name=\"SKGDateEdit\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>10</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGDateEditDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgdateedit.h");
}

