/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A table view with more features (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtableviewdesignerplugin.h"

#include <qicon.h>

#include "skgtableview.h"

SKGTableViewDesignerPlugin::SKGTableViewDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGTableViewDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGTableViewDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGTableViewDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGTableView(iParent);
}

QString SKGTableViewDesignerPlugin::name() const
{
    return QStringLiteral("SKGTableView");
}

QString SKGTableViewDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGTableViewDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("quickopen"));
}

QString SKGTableViewDesignerPlugin::toolTip() const
{
    return QStringLiteral("A table view with more features");
}

QString SKGTableViewDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A table view with more features");
}

bool SKGTableViewDesignerPlugin::isContainer() const
{
    return true;
}

QString SKGTableViewDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGTableView\" name=\"SKGTableView\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>100</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGTableViewDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgtableview.h");
}

