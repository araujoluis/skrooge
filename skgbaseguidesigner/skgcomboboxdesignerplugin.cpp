/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A combo box with more features (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgcomboboxdesignerplugin.h"

#include "skgcombobox.h"
#include "skgservices.h"

SKGComboBoxDesignerPlugin::SKGComboBoxDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGComboBoxDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGComboBoxDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGComboBoxDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGComboBox(iParent);
}

QString SKGComboBoxDesignerPlugin::name() const
{
    return QStringLiteral("SKGComboBox");
}

QString SKGComboBoxDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGComboBoxDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("quickopen"));
}

QString SKGComboBoxDesignerPlugin::toolTip() const
{
    return QStringLiteral("A combo box with more features");
}

QString SKGComboBoxDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A combo box with more features");
}

bool SKGComboBoxDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGComboBoxDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGComboBox\" name=\"SKGComboBox\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>10</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGComboBoxDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgcombobox.h");
}

