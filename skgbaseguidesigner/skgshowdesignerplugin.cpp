/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A widget to select what to show (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgshowdesignerplugin.h"

#include <qicon.h>

#include "skgservices.h"
#include "skgshow.h"

SKGShowDesignerPlugin::SKGShowDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGShowDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGShowDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGShowDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGShow(iParent);
}

QString SKGShowDesignerPlugin::name() const
{
    return QStringLiteral("SKGShow");
}

QString SKGShowDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGShowDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("quickopen"));
}

QString SKGShowDesignerPlugin::toolTip() const
{
    return QStringLiteral("A widget to select what to show");
}

QString SKGShowDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A widget to select what to show");
}

bool SKGShowDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGShowDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGShow\" name=\"SKGShow\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>10</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGShowDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgshow.h");
}

