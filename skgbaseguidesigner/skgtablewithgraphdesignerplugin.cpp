/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * A table with graph with more features (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgtablewithgraphdesignerplugin.h"

#include <qicon.h>



#include "skgtablewithgraph.h"

SKGTableWithGraphDesignerPlugin::SKGTableWithGraphDesignerPlugin(QObject* iParent)
    : QObject(iParent)
{
    m_initialized = false;
}

void SKGTableWithGraphDesignerPlugin::initialize(QDesignerFormEditorInterface*  iCore)
{
    Q_UNUSED(iCore)
    if (m_initialized) {
        return;
    }

    m_initialized = true;
}

bool SKGTableWithGraphDesignerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget* SKGTableWithGraphDesignerPlugin::createWidget(QWidget* iParent)
{
    return new SKGTableWithGraph(iParent);
}

QString SKGTableWithGraphDesignerPlugin::name() const
{
    return QStringLiteral("SKGTableWithGraph");
}

QString SKGTableWithGraphDesignerPlugin::group() const
{
    return QStringLiteral("SKG Widgets");
}

QIcon SKGTableWithGraphDesignerPlugin::icon() const
{
    return SKGServices::fromTheme(QStringLiteral("quickopen"));
}

QString SKGTableWithGraphDesignerPlugin::toolTip() const
{
    return QStringLiteral("A Table with graph");
}

QString SKGTableWithGraphDesignerPlugin::whatsThis() const
{
    return QStringLiteral("A Table with graph");
}

bool SKGTableWithGraphDesignerPlugin::isContainer() const
{
    return false;
}

QString SKGTableWithGraphDesignerPlugin::domXml() const
{
    return QStringLiteral("<widget class=\"SKGTableWithGraph\" name=\"SKGTableWithGraph\">\n"
                          " <property name=\"geometry\">\n"
                          "  <rect>\n"
                          "   <x>0</x>\n"
                          "   <y>0</y>\n"
                          "   <width>100</width>\n"
                          "   <height>100</height>\n"
                          "  </rect>\n"
                          " </property>\n"
                          "</widget>\n");
}

QString SKGTableWithGraphDesignerPlugin::includeFile() const
{
    return QStringLiteral("skgtablewithgraph.h");
}

