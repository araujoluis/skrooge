/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGCOMBOBOXDESIGNERPLUGIN_H
#define SKGCOMBOBOXDESIGNERPLUGIN_H
/** @file
 * A combo box with more features (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <QtUiPlugin/customwidget.h>

/**
 * QDesigner plugin for SKGComboBox
 */
class SKGComboBoxDesignerPlugin : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT
    Q_INTERFACES(QDesignerCustomWidgetInterface)

public:
    /**
    * Constructor
    * @param iParent parent
    */
    explicit SKGComboBoxDesignerPlugin(QObject* iParent = nullptr);

    /**
     * To know if the component is a container
     * @return true or false
     */
    bool isContainer() const override;

    /**
     * To know if the component is initialized
     * @return true or false
     */
    bool isInitialized() const override;

    /**
     * To get the icon for this component
     * @return the icon
     */
    QIcon icon() const override;

    /**
     * To get the icon for this component
     * @return
     */
    QString domXml() const override;

    /**
     * To get the group for this component
     * @return group
     */
    QString group() const override;

    /**
     * To get the include file for this component
     * @return the include file
     */
    QString includeFile() const override;

    /**
     * To get the name for this component
     * @return name
     */
    QString name() const override;

    /**
     * To get the "tool tip" for this component
     * @return the "tool tip"
     */
    QString toolTip() const override;

    /**
     * To get the "whats this" for this component
     * @return the "whats this"
     */
    QString whatsThis() const override;

    /**
     * To get the widget representing the component
     * @param iParent the parent of the widget
     * @return the widget
     */
    QWidget* createWidget(QWidget* iParent) override;

    /**
     * Initilialize the component
     * @param iCore interface
     */
    void initialize(QDesignerFormEditorInterface* iCore) override;

private:
    bool m_initialized;
};

#endif
