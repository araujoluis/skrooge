/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGWIDGETCOLLECTIONDESIGNERPLUGIN_H
#define SKGWIDGETCOLLECTIONDESIGNERPLUGIN_H
/** @file
 * A collection of widgets (qt designer plugin).
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <QtUiPlugin/customwidget.h>


#include "skgbaseguidesigner_export.h"
/**
 * QDesigner plugin collection
 */
class SKGBASEGUIDESIGNER_EXPORT SKGWidgetCollectionDesignerPlugin: public QObject, public QDesignerCustomWidgetCollectionInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QDesignerCustomWidgetCollectionInterface")
    Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)

public:
    /**
     * Constructor
     * @param iParent the parent
     */
    explicit SKGWidgetCollectionDesignerPlugin(QObject* iParent = nullptr);

    /**
     * To get the list of widgets
     * @return the list of widgets
     */
    QList<QDesignerCustomWidgetInterface*> customWidgets() const override;

private:
    QList<QDesignerCustomWidgetInterface*> m_widgets;
};

#endif  // SKGWIDGETCOLLECTIONDESIGNERPLUGIN_H
