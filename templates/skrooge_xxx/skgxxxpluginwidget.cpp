/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * ##DESCRIPTION##.
 *
 * @author ##AUTHOR##
 */
#include "skgxxxpluginwidget.h"

#include <qdom.h>

#include "skgmainpanel.h"
#include "skgtraces.h"
#include "skgdocument.h"

SKGXXXPluginWidget::SKGXXXPluginWidget(QWidget* iParent, SKGDocument* iDocument)
    : SKGTabPage(iParent, iDocument)
{
    SKGTRACEINFUNC(1)
    if (!iDocument) {
        return;
    }

    ui.setupUi(this);

    // Build you panel here
}

SKGXXXPluginWidget::~SKGXXXPluginWidget()
{
    SKGTRACEINFUNC(1)
}

QString SKGXXXPluginWidget::getState()
{
    SKGTRACEINFUNC(10)
    QDomDocument doc("SKGML");
    QDomElement root = doc.createElement("parameters");
    doc.appendChild(root);

    // Get state
    // Example: QString account=root.attribute ( "account");
    //         if (account.isEmpty()) root.setAttribute(QStringLiteral("account"), ui.kDisplayAccountCombo->currentText());

    return doc.toString();
}

void SKGXXXPluginWidget::setState(const QString& iState)
{
    SKGTRACEINFUNC(10)
    QDomDocument doc("SKGML");
    doc.setContent(iState);
    QDomElement root = doc.documentElement();

    // Set state
    // Example: QString account=root.attribute ( "account");
}

QString SKGXXXPluginWidget::getDefaultStateAttribute()
{
    return "";
}

QWidget* SKGXXXPluginWidget::mainWidget()
{
    return this;
}

void SKGXXXPluginWidget::refresh()

{
    SKGTRACEINFUNC(1)

    QSqlDatabase* db = getDocument()->getDatabase();
    setEnabled(db != nullptr);
    if (db != nullptr) {
        // Refresh yours widgets here
    }
}




