/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * ##DESCRIPTION##
 *
 * @author ##AUTHOR##
 */
#include "skgxxxplugin.h"

#include <kactioncollection.h>
#include <kstandardaction.h>
#include <kaboutdata.h>
#include <kpluginfactory.h>

#include "skgxxxpluginwidget.h"
#include "skgxxx_settings.h"
#include "skgtraces.h"

/**
 * This plugin factory.
 */
K_PLUGIN_FACTORY(SKGXXXPluginFactory, registerPlugin<SKGXXXPlugin>();)

SKGXXXPlugin::SKGXXXPlugin(QWidget* iWidget, QObject* iParent, const QVariantList& /*iArg*/) :
    SKGInterfacePlugin(iParent), m_currentDocument(nullptr)
{
    Q_UNUSED(iWidget)
    SKGTRACEINFUNC(10)
}

SKGXXXPlugin::~SKGXXXPlugin()
{
    SKGTRACEINFUNC(10)
    m_currentDocument = nullptr;
}

bool SKGXXXPlugin::setupActions(SKGDocument* iDocument)
{
    SKGTRACEINFUNC(10)

    m_currentDocument = iDocument;

    setComponentName("skrooge_xxx", title());
    setXMLFile("skrooge_xxx.rc");

    // Create yours actions here
    return true;
}

void SKGXXXPlugin::refresh()
{
    SKGTRACEINFUNC(10)
}

SKGTabPage* SKGXXXPlugin::getWidget()
{
    SKGTRACEINFUNC(10)
    return new SKGXXXPluginWidget(SKGMainPanel::getMainPanel(), m_currentDocument);
}

QWidget* SKGXXXPlugin::getPreferenceWidget()
{
    SKGTRACEINFUNC(10)
    auto w = new QWidget();
    ui.setupUi(w);

    return w;
}

KConfigSkeleton* SKGXXXPlugin::getPreferenceSkeleton()
{
    return skgxxx_settings::self();
}

SKGError SKGXXXPlugin::savePreferences() const
{
    return SKGError();
}

QString SKGXXXPlugin::title() const
{
    return i18nc("The title", "xxx");  // TODO(You) MUST BE CHANGED
}

QString SKGXXXPlugin::icon() const
{
    return "dialog-information";  // TODO(You) MUST BE CHANGED
}

QString SKGXXXPlugin::toolTip() const
{
    return i18nc("The tool tip", "xxx");  // TODO(You) MUST BE CHANGED
}


int SKGXXXPlugin::getOrder() const
{
    return 999;
}

QStringList SKGXXXPlugin::tips() const
{
    QStringList output;
    output.push_back(i18nc("Description of a tips", "<p>... xxx is the best plugin of the world</p>"));  // TODO(You) MUST BE CHANGED
    return output;
}

bool SKGXXXPlugin::isInPagesChooser() const
{
    return true;
}

#include <skgxxxplugin.moc>


