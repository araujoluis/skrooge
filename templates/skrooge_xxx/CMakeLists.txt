#***************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 2 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program. If not, see <https://www.gnu.org/licenses/>  *
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_XXX ::..")

PROJECT(plugin_xxx)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_xxx_SRCS
	skgxxxplugin.cpp
	skgxxxpluginwidget.cpp)

ki18n_wrap_ui(skrooge_xxx_SRCS skgxxxpluginwidget_base.ui skgxxxpluginwidget_pref.ui)
kconfig_add_kcfg_files(skrooge_xxx_SRCS skgxxx_settings.kcfgc )

ADD_LIBRARY(skrooge_xxx MODULE ${skrooge_xxx_SRCS})
TARGET_LINK_LIBRARIES(skrooge_xxx KF5::Parts skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(TARGETS skrooge_xxx DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-plugin-xxx.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skrooge_xxx.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skrooge_xxx )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgxxx_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )

ECM_INSTALL_ICONS(${ICON_INSTALL_DIR})
