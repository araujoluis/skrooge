/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGXXXPLUGINWIDGET_H
#define SKGXXXPLUGINWIDGET_H
/** @file
 * ##DESCRIPTION##
*
* @author ##AUTHOR##
*/
#include "ui_skgxxxpluginwidget_base.h"
#include "skgtabpage.h"

/**
 * ##DESCRIPTION##
 */
class SKGXXXPluginWidget : public SKGTabPage
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGXXXPluginWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    virtual ~SKGXXXPluginWidget();

    /**
     * Get the current state
     * MUST BE OVERWRITTEN
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    virtual QString getState() override;

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    virtual void setState(const QString& iState) override;

    /**
     * Get attribute name to save the default state
     * MUST BE OVERWRITTEN
     * @return attribute name to save the default state.
     */
    virtual QString getDefaultStateAttribute() override;

    /**
     * Get the main widget
     * @return a widget
     */
    virtual QWidget* mainWidget() override;

public Q_SLOTS:
    /**
    * Refresh the content.
     */
    virtual void refresh();

private Q_SLOTS:


private:
    Q_DISABLE_COPY(SKGXXXPluginWidget)

    Ui::skgxxxplugin_base ui;
};

#endif  // SKGXXXPLUGINWIDGET_H
