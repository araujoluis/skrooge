/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file is Skrooge plugin for XXX import / export.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgimportpluginxxx.h"

#include <klocalizedstring.h>
#include <kpluginfactory.h>


#include "skgtraces.h"

/**
 * This plugin factory.
 */
K_PLUGIN_FACTORY(SKGImportPluginXXXFactory, registerPlugin<SKGImportPluginXXX>();)

SKGImportPluginXXX::SKGImportPluginXXX(QObject* iImporter, const QVariantList& iArg)
    : SKGImportPlugin(iImporter)
{
    SKGTRACEINFUNC(10)
    Q_UNUSED(iArg)
}

SKGImportPluginXXX::~SKGImportPluginXXX()
{
}

bool SKGImportPluginXXX::isImportPossible()
{
    SKGTRACEINFUNC(10)
    return (!m_importer ? true : m_importer->getFileNameExtension() == "XXX");
}

SKGError SKGImportPluginXXX::importFile()
{
    return SKGImportPlugin::importFile();
}

bool SKGImportPluginXXX::isExportPossible()
{
    SKGTRACEINFUNC(10)
    return isImportPossible();
}

SKGError SKGImportPluginXXX::exportFile()
{
    return SKGImportPlugin::exportFile();
}

QString SKGImportPluginXXX::getMimeTypeFilter() const
{
    return "*.xxx|" % i18nc("A file format", "XXX file");
}


