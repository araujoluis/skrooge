/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGDEFINEBANK_H
#define SKGDEFINEBANK_H
/** @file
 * This file defines some macros and constants.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgdefine.h"

/**
 * @var DUMPUNIT
 * To display units and values
 * @see dump
 */
static const int DUMPUNIT = (2u << 10);

/**
 * @var DUMPACCOUNT
 * To display accounts
 * @see dump
 */
static const int DUMPACCOUNT = (2u << 11);

/**
 * @var DUMPOPERATION
 * To display accounts
 * @see dump
 */
static const int DUMPOPERATION = (2u << 12);

/**
 * @var DUMPCATEGORY
 * To display categories
 * @see dump
 */
static const int DUMPCATEGORY = (2u << 13);

/**
 * @var DUMPPAYEE
 * To display payees
 * @see dump
 */
static const int DUMPPAYEE = (2u << 14);

/**
 * @var DUMPBUDGET
 * To display payees
 * @see dump
 */
static const int DUMPBUDGET = (2u << 15);

/**
 * @var DUMPBANKOBJECT
 * To display categories
 * @see dump
 */
static const int DUMPBANKOBJECT = DUMPUNIT | DUMPACCOUNT | DUMPOPERATION | DUMPCATEGORY | DUMPPAYEE | DUMPBUDGET;
#endif
