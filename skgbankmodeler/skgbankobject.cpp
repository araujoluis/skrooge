/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
/** @file
 * This file implements classes SKGBankObject.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgbankobject.h"

#include <klocalizedstring.h>

#include "skgaccountobject.h"
#include "skgdocumentbank.h"

SKGBankObject::SKGBankObject(): SKGBankObject(nullptr)
{}

SKGBankObject::SKGBankObject(SKGDocument* iDocument, int iID): SKGNamedObject(iDocument, QStringLiteral("v_bank"), iID)
{}

SKGBankObject::~SKGBankObject()
    = default;

SKGBankObject::SKGBankObject(const SKGBankObject& iObject)
    = default;

SKGBankObject::SKGBankObject(const SKGNamedObject& iObject)
{
    if (iObject.getRealTable() == QStringLiteral("bank")) {
        copyFrom(iObject);
    } else {
        *this = SKGNamedObject(iObject.getDocument(), QStringLiteral("v_bank"), iObject.getID());
    }
}

SKGBankObject::SKGBankObject(const SKGObjectBase& iObject)
{
    if (iObject.getRealTable() == QStringLiteral("bank")) {
        copyFrom(iObject);
    } else {
        *this = SKGNamedObject(iObject.getDocument(), QStringLiteral("v_bank"), iObject.getID());
    }
}

SKGBankObject& SKGBankObject::operator= (const SKGObjectBase& iObject)
{
    copyFrom(iObject);
    return *this;
}

SKGBankObject& SKGBankObject::operator= (const SKGBankObject& iObject)
{
    copyFrom(iObject);
    return *this;
}

SKGError SKGBankObject::addAccount(SKGAccountObject& oAccount)
{
    SKGError err;
    if (getID() == 0) {
        err = SKGError(ERR_FAIL, i18nc("Error message", "%1 failed because linked object is not yet saved in the database.", QStringLiteral("SKGBankObject::addAccount")));
    } else {
        oAccount = SKGAccountObject(qobject_cast<SKGDocumentBank*>(getDocument()));
        err = oAccount.setAttribute(QStringLiteral("rd_bank_id"), SKGServices::intToString(getID()));
    }
    return err;
}

SKGError SKGBankObject::getAccounts(SKGListSKGObjectBase& oAccountList) const
{
    SKGError err = getDocument()->getObjects(QStringLiteral("v_account"),
                   "rd_bank_id=" % SKGServices::intToString(getID()),
                   oAccountList);
    return err;
}

SKGError SKGBankObject::setNumber(const QString& iNumber)
{
    return setAttribute(QStringLiteral("t_bank_number"), iNumber);
}

QString SKGBankObject::getNumber() const
{
    return getAttribute(QStringLiteral("t_bank_number"));
}

SKGError SKGBankObject::setIcon(const QString& iIcon)
{
    return setAttribute(QStringLiteral("t_icon"), iIcon);
}

QString SKGBankObject::getIcon() const
{
    return getAttribute(QStringLiteral("t_icon"));
}

double SKGBankObject::getCurrentAmount() const
{
    return SKGServices::stringToDouble(getAttributeFromView(QStringLiteral("v_bank_amount"), QStringLiteral("f_CURRENTAMOUNT")));
}

