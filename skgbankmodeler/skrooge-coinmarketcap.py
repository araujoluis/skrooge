#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#**************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr
#*   Redistribution and use in source and binary forms, with or without
#*   modification, are permitted provided that the following conditions
#*   are met:
#*   
#*   1. Redistributions of source code must retain the above copyright
#*      notice, this list of conditions and the following disclaimer.
#*   2. Redistributions in binary form must reproduce the above copyright
#*      notice, this list of conditions and the following disclaimer in the
#*      documentation and/or other materials provided with the distribution.
#*   
#*   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#*   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
#*   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#*   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
#*   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
#*   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#*   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#*   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
#*   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#**************************************************************************

from urllib.request import Request, urlopen
import json
import sys

units=sys.argv[1].split('-')
# url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/historical?symbol='+units[0]+'&convert='+units[1]+'&time_start='+sys.argv[2]+'T00:00:00.000Z&time_end='+sys.argv[3]+'T23:59:59.000Z'
url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol='+units[0]+'&convert='+units[1]

req = Request(url)
req.add_header("X-CMC_PRO_API_KEY", sys.argv[5])
f = urlopen(req)
print("Date,Price")
item=json.loads(f.read().decode('utf-8'))['data'][units[0]]['quote'][units[1]]
print(item["last_updated"][0:10]+','+str(item["price"]))
