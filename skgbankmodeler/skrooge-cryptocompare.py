#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#**************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr
#*   Redistribution and use in source and binary forms, with or without
#*   modification, are permitted provided that the following conditions
#*   are met:
#*   
#*   1. Redistributions of source code must retain the above copyright
#*      notice, this list of conditions and the following disclaimer.
#*   2. Redistributions in binary form must reproduce the above copyright
#*      notice, this list of conditions and the following disclaimer in the
#*      documentation and/or other materials provided with the distribution.
#*   
#*   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#*   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
#*   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#*   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
#*   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
#*   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#*   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#*   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
#*   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#**************************************************************************

import urllib.request
import json
import sys
import datetime

units=sys.argv[1].split('-')
mode = sys.argv[4]

nb = min((datetime.datetime.strptime(sys.argv[2], '%Y-%m-%d')-datetime.datetime.strptime(sys.argv[3], '%Y-%m-%d')).days, 2000)
url = 'https://min-api.cryptocompare.com/data/v2/histoday?fsym='+units[0]+'&tsym='+units[1]+'&limit='+str(nb)+'&api_key='+sys.argv[5]
f = urllib.request.urlopen(url)
print("Date,Price")
for item in json.loads(f.read().decode('utf-8'))['Data']['Data']:
    d = datetime.datetime.fromtimestamp(int(item["time"]))
    if mode == '1d' or (mode=='1wk' and d.isoweekday()==1) or (mode=='1mo' and d.day==1):
        print(d.strftime('%Y-%m-%d')+','+str(item['close']))
