/***************************************************************************
 *   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program. If not, see <https://www.gnu.org/licenses/>  *
 ***************************************************************************/
#ifndef SKGIMPORTPLUGIN_H
#define SKGIMPORTPLUGIN_H
/** @file
* This file is a plugin interface definition.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include <kparts/plugin.h>

#include "skgbankmodeler_export.h"
#include "skgerror.h"
#include "skgimportexportmanager.h"


/**
 * This file is a plugin interface definition.
 */
class SKGBANKMODELER_EXPORT SKGImportPlugin : public KParts::Plugin
{
    Q_OBJECT
public:
    /**
     * Default constructor
     * @param iImporter the parent importer
     */
    explicit SKGImportPlugin(QObject* iImporter = nullptr);

    /**
     * Default destructor
     */
    ~SKGImportPlugin() override;

    /**
     * Get parameters for Import
     * @return the parameters
     */
    virtual inline QMap<QString, QString> getImportParameters()
    {
        return m_importParameters;
    }

    /**
     * Set parameters for Import
     * @param iParameters the parameters
     */
    virtual inline void setImportParameters(const QMap<QString, QString>& iParameters)
    {
        m_importParameters = iParameters;
    }

    /**
     * Get parameters for Export
     * @return the parameters
     */
    virtual inline QMap<QString, QString> getExportParameters()
    {
        return m_exportParameters;
    }

    /**
     * Set parameters for Export
     * @param iParameters the parameters
     */
    virtual inline void setExportParameters(const QMap<QString, QString>& iParameters)
    {
        m_exportParameters = iParameters;
    }

    /**
     * To know if import is possible with this plugin
     * @return true or false
     */
    virtual inline bool isImportPossible()
    {
        return false;
    }

    /**
     * Import a file
     * @return an object managing the error.
     *   @see SKGError
     */
    virtual inline SKGError importFile()
    {
        return SKGError(ERR_NOTIMPL, QLatin1String(""));
    }

    /**
     * To know if export is possible with this plugin
     * @return true or false
     */
    virtual inline bool isExportPossible()
    {
        return false;
    }

    /**
     * Export a file
     * @return an object managing the error.
     *   @see SKGError
     */
    virtual inline SKGError exportFile()
    {
        return SKGError(ERR_NOTIMPL, QLatin1String(""));
    }

    /**
     * Return the mime type filter
     * @return the mime type filter. Example: "*.csv|CSV file"
     */
    virtual QString getMimeTypeFilter() const
    {
        return QLatin1String("");
    }

private:
    Q_DISABLE_COPY(SKGImportPlugin)

protected:
    SKGImportExportManager* m_importer;
    QMap<QString, QString> m_importParameters;
    QMap<QString, QString> m_exportParameters;
};

/**
 * This plugin interface definition.
 */
Q_DECLARE_INTERFACE(SKGImportPlugin, "skrooge.com.SKGImportPlugin/1.0")

#endif  // SKGIMPORTPLUGIN_H
