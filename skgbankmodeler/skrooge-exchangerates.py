#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#**************************************************************************
#*   Copyright (C) 2020 by S. MANKOWSKI / G. DE BURE support@mankowski.fr
#*   Redistribution and use in source and binary forms, with or without
#*   modification, are permitted provided that the following conditions
#*   are met:
#*   
#*   1. Redistributions of source code must retain the above copyright
#*      notice, this list of conditions and the following disclaimer.
#*   2. Redistributions in binary form must reproduce the above copyright
#*      notice, this list of conditions and the following disclaimer in the
#*      documentation and/or other materials provided with the distribution.
#*   
#*   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#*   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
#*   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#*   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
#*   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
#*   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#*   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#*   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#*   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
#*   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#**************************************************************************

import urllib.request
import json
import sys
import datetime

units=sys.argv[1].split('-')
mode = sys.argv[4]

url = 'https://api.exchangeratesapi.io/history?start_at='+sys.argv[3]+'&end_at='+sys.argv[2]+'&symbols='+units[1]+'&base='+units[0]
f = urllib.request.urlopen(url)
print("Date,Price")
data = json.loads(f.read().decode('utf-8'))['rates']

output = []
for item in data:
    d = datetime.datetime.strptime(item, '%Y-%m-%d')
    if mode == '1d' or (mode=='1wk' and d.isoweekday()==1) or (mode=='1mo' and d.day==1) or (mode=='1mo' and d.day==2 and not datetime.date(d.year, d.month, d.day-1).strftime('%Y-%m-%d') in data) or (mode=='1mo' and d.day==3 and not datetime.date(d.year, d.month, d.day-1).strftime('%Y-%m-%d') in data and not datetime.date(d.year, d.month, d.day-2).strftime('%Y-%m-%d') in data):
        output.append(item+","+str(data[item][units[1]]))

for item in sorted(output):
    print(item)
